package oracle_test

import (
	"fmt"
	"log/slog"
	"os"
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/oracle"
)

func Example() {
	// Translate a query from Oracle to PostgreSQL
	sql, err := oracle.Translate("<stdin>", "SELECT SYSDATE FROM SYS.DUAL;")
	if err != nil {
		panic(err)
	}
	fmt.Println(sql)
	// Output: SELECT LOCALTIMESTAMP;
}

func TestMain(m *testing.M) {
	slog.SetDefault(slog.New(slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
		Level: slog.LevelDebug,
	})))
	os.Exit(m.Run())
}

func TestNoChanges(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT 1;")
	r.NoError(err)
	r.Equal("SELECT 1;", sql)
}

func TestNvl(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT NVL(1, 2);")
	r.NoError(err)
	r.Equal("SELECT COALESCE(1, 2);", sql)
}

func TestSysdateFromDual(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT sysdate FROM DUAL;")
	r.NoError(err)
	r.Equal("SELECT localtimestamp;", sql)
}

func TestRemoveLastToDateArgument(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT TO_DATE('2018-01-01', 'YYYY-MM-DD', 'NLS_DATE_LANGUAGE=ENGLISH') FROM DUAL;")
	r.NoError(err)
	r.Equal("SELECT TO_DATE('2018-01-01', 'YYYY-MM-DD');", sql)
}

func TestAddToDateFormat(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "TO_DATE('14-DEC-38');")
	r.NoError(err)
	r.Equal("TO_DATE('14-DEC-38', 'DD-MON-YY');", sql)
}

func TestRemoveFromSysDual(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT user FROM sys.dual;")
	r.NoError(err)
	r.Equal("SELECT user;", sql)
}

func TestRemoveFromSysNamespace(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT table_name FROM sys.user_tables, sys.user_tab_columns;")
	r.NoError(err)
	r.Equal("SELECT table_name FROM user_tables, user_tab_columns;", sql)
}

func TestMoveHavingAfterGroupBy(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT 1 FROM t HAVING 2 GROUP BY 3;")
	r.NoError(err)
	r.Equal("SELECT 1 FROM t GROUP BY 3 HAVING 2;", sql)

	// With spacings
	sql, err = oracle.Translate("<stdin>", dedent.Dedent(`
	SELECT 1
	FROM t
	-- having comment
	HAVING 2
	-- group by comment
	GROUP BY 3;
	`))
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	SELECT 1
	FROM t
	-- group by comment
	GROUP BY 3
	-- having comment
	HAVING 2;
	`), sql)
}

func TestMinus(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT 1 minus select 2;")
	r.NoError(err)
	r.Equal("SELECT 1 except select 2;", sql)
}
