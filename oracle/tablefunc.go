package oracle

import (
	"gitlab.com/dalibo/transqlate/internal/ast"
	"gitlab.com/dalibo/transqlate/internal/parser"
)

type tableFunc struct{}

func (tableFunc) String() string {
	return "rewrite TABLE function call"
}

func (tableFunc) Match(node ast.Node) bool {
	prefix, _ := node.(ast.Prefix)
	return prefix.Is("TABLE")
}

func (tableFunc) Rewrite(node ast.Node) (ast.Node, error) {
	prefix := node.(ast.Prefix)
	function := prefix.Expression.(ast.Grouping).Items[0].Expression
	call, ok := function.(ast.Call)
	if !ok {
		call = ast.Call{
			Function: function,
			Args:     parser.MustParse("()").(ast.Grouping),
		}
	}
	return ast.SpaceFrom(call, node), nil
}
