// Package oracle transpiles from Oracle to PostgreSQL.
//
// The main entrypoint is oracle.Translate for simple case or oracle.Engine for more control and
// reusability.
package oracle
