package oracle

import (
	"fmt"
	"strings"

	"gitlab.com/dalibo/transqlate/lexer"
)

type identifierCase struct {
	preserveCase bool
}

func (r identifierCase) String() string {
	if r.preserveCase {
		return "preserve case"
	}
	return "change case"
}

func (i identifierCase) Rewrite(token lexer.Token) lexer.Token {
	if token.Type != lexer.Identifier {
		return token
	}

	if isMixedCase(token.Str) && token.Quoted() {
		// Always keep "MixedCase" as is.
		return token
	}

	if i.preserveCase {
		if !token.Quoted() {
			// Quote AB -> "AB".
			token.Set(fmt.Sprintf(`"%s"`, strings.ToUpper(token.Raw)))
		}
	} else {
		// Everything else as unquoted. Let Pretty effectively lower identifiers.
		token.Set(lexer.UnquoteIdentifier(token.Raw))
		// Ensure unquoted identifier is not lexed as a keyword.
		if lexer.IsKeyword(token.Raw) {
			token.Set(lexer.QuoteIdentifier(token.Raw))
		}
	}
	return token
}

func isMixedCase(s string) bool {
	return strings.ContainsAny(s, "ABCDEFGHIJKLMNOPQRSTUVWXYZ") && strings.ContainsAny(s, "abcdefghijklmnopqrstuvwxyz")
}
