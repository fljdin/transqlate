package oracle_test

import (
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/oracle"
)

func TestDecode(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", dedent.Dedent(`
	-- comment
	decode(1, 2, 3, 4)
	`))
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	-- comment
	CASE 1 WHEN 2 THEN 3 ELSE 4 END
	`), sql)
}

func TestDecodeIndented(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", dedent.Dedent(`
	SELECT decode(
		1,
		2, 3,
		4, 5
	)
	`))
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	SELECT CASE 1
		WHEN 2 THEN 3
		WHEN 4 THEN 5
	END
	`), sql)
}
