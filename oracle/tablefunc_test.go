package oracle_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/oracle"
)

func TestTableFuncWithArgs(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT * FROM TABLE (function(1, 2));")
	r.NoError(err)
	r.Equal("SELECT * FROM function(1, 2);", sql)
}

func TestTableFuncWithoutArgs(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT * FROM TABLE (function);")
	r.NoError(err)
	r.Equal("SELECT * FROM function();", sql)
}
