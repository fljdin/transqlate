package oracle

import (
	"fmt"

	"gitlab.com/dalibo/transqlate/internal/ast"
	"gitlab.com/dalibo/transqlate/internal/joinmark"
	"gitlab.com/dalibo/transqlate/internal/parser"
	"gitlab.com/dalibo/transqlate/internal/rules"
	"gitlab.com/dalibo/transqlate/internal/startwith"
	"gitlab.com/dalibo/transqlate/lexer"
	"gitlab.com/dalibo/transqlate/rewrite"
)

// Translate transpile Oracle SQL into Postgres SQL.
//
// SQL may be returned with errors if some rules failed. This allows to know whether the translation
// is complete or not and why.
// Use rewrite.JoinedErrors.Unwrap() to get the list of errors.
//
// Wraps lexing, parsing and rewrite engine initialization and execution.
//
// Without options, Defaults() applies. Otherwise, you can explicitly call Defaults().
//
// See Example for a sample usage.
func Translate(source, sql string, o ...rewrite.Option) (string, error) {
	e := Engine(o...)
	tokens, err := e.Translate(source, sql)
	sql = lexer.Write(tokens)
	return sql, err
}

// Engine creates a new engine for Oracle to Postgres translation.
//
// The Engine is responsible to translate an already parsed code.
//
// Without options, Defaults() applies. Otherwise, you can explicitly call Defaults().
func Engine(options ...rewrite.Option) rewrite.Engine {
	engine := rewrite.New("oracle", rewrite.Parser(parse))

	engine.Append(
		// Order matters here.
		removeSysNamespace{},
		removeFromDual{},
		// Assume lexer.UpperIdentifiers() is called. Identifiers will be compared uppercased.
		rules.RenameFunction{From: "NVL", To: "COALESCE"},
		rules.ReplaceOperator{From: []string{"MINUS"}, To: []string{"EXCEPT"}},
		moveHavingAfterGroupBy{},
		rewriteDecodeAsCase{},
		tableFunc{},
		joinmark.Rule{},
		startwith.Rule{},
		// Date rules.
		rules.RenameConstant{From: "SYSDATE", To: "LOCALTIMESTAMP"},
		rules.RenameConstant{From: "SYSTIMESTAMP", To: "CURRENT_TIMESTAMP"},
		replaceAddMonths{},
		replaceTrunc{},
		rules.RemoveLastArgument{Function: "TO_DATE", Position: 2},
	)

	if len(options) == 0 {
		options = append(options, Defaults())
	}

	engine.Apply(options...)

	return engine
}

// Defaults configures an engine with transqlate defaults.
//
// It applies the following options:
//
//   - DefaultDateFormat("DD-MON-YY")
//   - Lower objects identifiers.
//
// Defaults is incompatible with PreserveCase.
//
// Combine this with custom options if you add options to oracle.Translate.
func Defaults() rewrite.Option {
	return func(e *rewrite.Engine) {
		// See. https://docs.oracle.com/en/database/oracle/oracle-database/19/sqlrf/TO_DATE.html#GUID-D226FA7C-F7AD-41A0-BB1D-BD8EF9440118
		// this is the default value for US NLS_TERRITORY.
		e.Apply(DefaultDateFormat("DD-MON-YY"))
		e.Append(
			rules.CallKeyword{},
			identifierCase{},
		)
	}
}

// DefaultDateFormat sets the default format for to_date function.
//
// Use this to define default date format according to NLS_TERRITORY of origin Oracle server.
//
// If you don't use this, transqlate defaults to "DD-MON-YY" date format, i.e. US NLS_TERRITORY.
func DefaultDateFormat(format string) rewrite.Option {
	format = fmt.Sprintf(` '%s'`, format)
	return func(e *rewrite.Engine) {
		e.Append(
			rules.AddMissingArgument{
				// Identifier is tested lowered. Position is 0 based.
				Function: "TO_DATE", Position: 1, Value: parser.MustParse(format),
			},
		)
	}
}

// PreserveCase assumes objects will be uppercased in Postgres.
//
// PreserveCase is incompatible with Defaults.
//
// Uppercased identifiers will be quoted if not.
func PreserveCase(v bool) rewrite.Option {
	return func(e *rewrite.Engine) {
		e.Append(
			rules.CallKeyword{},
			// It's a token rule, always executed after node rules.
			identifierCase{preserveCase: v},
		)
		if v {
			e.Append(
				rules.QuoteBuiltins{},
			)
		}
	}
}

// parse Oracle SQL snippet into an AST.
func parse(source, input string) (ast.Node, error) {
	lexer.UpperIdentifiers()
	return parser.Parse(lexer.New(source, input))
}
