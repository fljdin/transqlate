package oracle

import (
	"fmt"
	"log/slog"
	"strings"

	"gitlab.com/dalibo/transqlate/internal/ast"
	"gitlab.com/dalibo/transqlate/internal/parser"
	"gitlab.com/dalibo/transqlate/lexer"
)

type replaceTrunc struct{}

func (replaceTrunc) String() string {
	return "replace trunc() by date_trunc()"
}

func (replaceTrunc) Match(n ast.Node) bool {
	c, _ := n.(ast.Call)
	return c.Is("TRUNC")
}

func (replaceTrunc) Rewrite(n ast.Node) (ast.Node, error) {
	c := n.(ast.Call)
	f := c.Function.(ast.Leaf)
	f.Token.Set(lexer.ApplyCase(f.Token.Raw, "DATE_TRUNC"))
	c.Function = f

	if len(c.Args.Items) == 1 {
		slog.Info("TRUNC() with single parameter. Assuming day.", "line", 1+f.Token.Line, "column", 1+f.Token.Column)
		c.Args.Append(parser.MustParse(" 'DD'"))
	}

	// Swap arguments.
	c.Args.Items[0].Expression, c.Args.Items[1].Expression = c.Args.Items[1].Expression, c.Args.Items[0].Expression

	// Translate date format.
	f, _ = c.Args.Items[0].Expression.(ast.Leaf)
	if f.Token.Type != lexer.String {
		c.Args = spaceDateTruncArgs(c.Args)
		return c, fmt.Errorf("not a literal format")
	}

	datefmt := translateDateFormat(f.Token.Str)
	if datefmt == "" {
		c.Args.Items[0].Expression = f
		c.Args = spaceDateTruncArgs(c.Args)
		return c, fmt.Errorf("unsupported date format: %q", f.Token.Str)
	}
	f.Token.Set(lexer.QuoteString(datefmt))
	c.Args.Items[0].Expression = f

	c.Args = spaceDateTruncArgs(c.Args)
	return c, nil
}

// translateDateFormat rewrites Oracle date model to PostgreSQL date format
//
// https://docs.oracle.com/cd/B19306_01/server.102/b14200/sql_elements004.htm#i34924
// https://www.postgresql.org/docs/current/functions-formatting.html#FUNCTIONS-FORMATTING-DATETIME-TABLE
//
// Returns an empty string if the format is not supported.
func translateDateFormat(m string) string {
	m = strings.ToUpper(m)
	switch m {
	case "CC", "SCC":
		return "century"
	case "SYYYY", "YYYY", "YEAR", "SYEAR", "YYY", "YY", "Y":
		return "year"
	case "Q":
		return "quarter"
	case "MONTH", "MON", "MM", "RM":
		return "month"
	case "IW":
		return "week"
	case "DDD", "DD", "J":
		return "day"
	case "HH", "HH12", "HH24":
		return "hour"
	case "MI":
		return "minute"
	default:
		return "" // Unsupported
	}
}

func spaceDateTruncArgs(args ast.Grouping) ast.Grouping {
	args.Items[0].Expression, args.Items[1].Expression = ast.SwapWhitespaces(args.Items[0].Expression, args.Items[1].Expression)
	return args
}
