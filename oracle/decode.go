package oracle

import (
	"gitlab.com/dalibo/transqlate/internal/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

type rewriteDecodeAsCase struct{}

func (rewriteDecodeAsCase) String() string {
	return "rewrite DECODE() as CASE"
}

func (rewriteDecodeAsCase) Match(n ast.Node) bool {
	c, _ := n.(ast.Call)
	if len(c.Args.Items) < 3 {
		return false
	}
	return c.Is("DECODE")
}

func (rewriteDecodeAsCase) Rewrite(n ast.Node) (ast.Node, error) {
	args := n.(ast.Call).Args
	c := ast.Case{
		Case:       lexer.MustLex("CASE")[0],
		Expression: args.Items[0].Expression,
		End:        lexer.MustLex(" END")[0],
	}

	// Translate WHEN and THEN pairs.
	items := args.Items[1:]
	for len(items) > 1 {
		w := ast.When{
			When: ast.Prefix{
				Token:      lexer.MustLex(" WHEN")[0],
				Expression: items[0].Expression,
			},
			Then: ast.Prefix{
				Token:      lexer.MustLex(" THEN")[0],
				Expression: items[1].Expression,
			},
		}
		c.Cases = append(c.Cases, w)
		items = items[2:]
	}

	// Put trailing item into ELSE.
	if len(items) == 1 {
		c.Else = ast.Prefix{
			Token:      lexer.MustLex("ELSE")[0],
			Expression: items[0].Expression,
		}
	}

	return spaceCaseFromDecode(c, n.(ast.Call)), nil
}

// spaceCaseFromDecode sets indentation for CASE expression.
//
// Preserves indentation of DECODE function and its arguments.
func spaceCaseFromDecode(c ast.Case, decode ast.Call) ast.Node {
	c = ast.SpaceFrom(c, decode)

	// Pad one space between CASE and expression.
	c.Expression = ast.SpaceFromItem(c.Expression, decode.Args, 0)
	c.Expression = ast.OneSpace(c.Expression)

	for i, w := range c.Cases {
		w.When = ast.SpaceFromItem(w.When, decode.Args, 1+i*2)
		w.When.Expression = ast.OneSpace(w.When.Expression)
		w.Then = ast.SpaceFromItem(w.Then, decode.Args, 1+i*2+1)
		w.Then.Expression = ast.OneSpace(w.Then.Expression)
		c.Cases[i] = w
	}
	if c.Else.Is("ELSE") {
		c.Else = ast.SpaceFromItem(c.Else, decode.Args, len(decode.Args.Items)-1)
	}
	c.End = c.End.SpaceFrom(decode.Args.Close)
	if !decode.Args.Close.BeginsLine() {
		// Ensure space between last expression and END.
		c.End.Prefix = " "
	}
	return c
}
