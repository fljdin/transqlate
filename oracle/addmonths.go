package oracle

import (
	"gitlab.com/dalibo/transqlate/internal/ast"
	"gitlab.com/dalibo/transqlate/internal/parser"
	"gitlab.com/dalibo/transqlate/lexer"
)

type replaceAddMonths struct{}

func (replaceAddMonths) String() string {
	return "replace add_months() by interval calculation"
}

func (replaceAddMonths) Match(n ast.Node) bool {
	c, _ := n.(ast.Call)
	return c.Is("ADD_MONTHS")
}

// add_months(date, count) -> date + interval '<count> months'
func (replaceAddMonths) Rewrite(n ast.Node) (ast.Node, error) {
	c := n.(ast.Call)

	// Final node is an addition which is an infix operation.
	i := ast.Infix{
		Left: c.Args.Items[0].Expression,
		Op:   lexer.MustLex(" +"),
	}

	// Second argument is a number count to add/subtract.
	// Should be a (negative) Prefix node or a (positive) Leaf node.
	count := c.Args.Items[1].Expression
	operation, ok := count.(ast.Prefix)
	if ok {
		i.Op[0] = operation.Token
		count = operation.Expression
	}
	value, _ := count.(ast.Leaf)

	if value.Token.Type == lexer.Integer {
		// Simplest case, just add the number of months.
		i.Right = parser.MustParse(" interval '%s %s'",
			value.Token.Raw,
			pluralize(value.Token.Int, "month", "months"),
		)
	} else {
		// Fallback to a multiplication of operand.
		newCount := parser.MustParse(" 'count' * interval '1 month'").(ast.Infix)
		newCount.Left = count
		i.Right = ast.SpaceFrom(newCount, count)
	}

	return ast.SpaceFrom(i, n), nil
}
