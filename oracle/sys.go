package oracle

import (
	"gitlab.com/dalibo/transqlate/internal/ast"
)

type removeSysNamespace struct{}

func (removeSysNamespace) String() string {
	return "remove SYS."
}

func (removeSysNamespace) Match(n ast.Node) bool {
	i, _ := n.(ast.Infix)
	if !i.Is(".") {
		return false
	}

	l, _ := i.Left.(ast.Leaf)
	return l.In("SYS")
}

func (removeSysNamespace) Rewrite(n ast.Node) (ast.Node, error) {
	i := n.(ast.Infix)
	sys := i.Left.(ast.Leaf).Token

	r := i.Right.(ast.Leaf)
	r.Token.Prefix = sys.Prefix

	return r, nil
}
