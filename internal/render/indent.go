package render

import (
	"strings"
)

type Indenter struct {
	current int
	builder strings.Builder
}

func (i *Indenter) AppendLine(s string) {
	i.builder.WriteString(colorIndentation(strings.Repeat("┊ ", i.current)) + s + "\n")
}

func (i *Indenter) Indent() {
	i.current++
}

func (i *Indenter) Dedent() {
	i.current--
}

func (i *Indenter) String() string {
	return i.builder.String()
}
