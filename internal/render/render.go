// Package render presents AST, traces and SQL
package render

import (
	"strings"

	"gitlab.com/dalibo/transqlate/lexer"
)

// Renumber updates the position of tokens to be relative to the previous token.
//
// This helps to keep track of the position of tokens in the new input when reporting error in code.
func Renumber(previous lexer.Token, tokens lexer.Tokener) (out lexer.Tokens) {
	for _, t := range tokens.Tokens() {
		if t.IsZero() {
			continue
		}
		previousCode := previous.Raw + previous.Suffix + t.Prefix
		t.Line = previous.Line + strings.Count(previousCode, "\n")
		lastNewline := strings.LastIndex(previousCode, "\n")
		if lastNewline == -1 {
			t.Column = previous.Column + len(previousCode)
		} else {
			t.Column = len(previousCode[lastNewline+1:])
		}
		previous = t
		out = append(out, t)
	}
	return out
}
