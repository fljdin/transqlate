package render

import (
	"strings"

	"gitlab.com/dalibo/transqlate/lexer"
)

// Highlight returns a copy of the input tokens with colorized syntax.
//
// Highlights erroring tokens. An erroring token is determined by NewErrorHinter. See CommentError
// to show errors.
func Highlight(tokens lexer.Tokener) (out lexer.Tokens) {
	hinter := NewErrorHinter(tokens)
	wasError := false
	for {
		t, inError := hinter.Next()
		if t.IsZero() {
			break
		}

		if inError {
			if t.LeadingComment() == "" && !strings.Contains(t.Prefix, "\n") && !t.BeginsLine() && wasError {
				t.Prefix = colorError(t.Prefix)
			} else {
				t.Prefix = colorComment(t.Prefix)
			}
			t.Raw = colorError(t.Raw)
			if t.TrailingComment() == "" && !strings.Contains(t.Suffix, "\n") {
				t.Suffix = colorError(t.Suffix)
			} else {
				t.Suffix = colorComment(t.Suffix)
			}
		} else {
			t.Prefix = colorComment(t.Prefix)
			switch t.Type {
			case lexer.Keyword:
				t.Raw = colorKeyword(t.Raw)
			case lexer.String:
				t.Raw = colorString(t.Raw)
			case lexer.Identifier:
				t.Raw = colorIdentifier(t.Raw)
			case lexer.Integer, lexer.Float:
				t.Raw = colorConstant(t.Raw)
			}
			t.Suffix = colorComment(t.Suffix)
		}
		out = append(out, t)
		wasError = inError
	}

	return
}
