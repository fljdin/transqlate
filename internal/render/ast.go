package render

import (
	"fmt"
	"reflect"

	"gitlab.com/dalibo/transqlate/internal/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

// Ast serialize a tree of structs and lists.
//
// Skips fields marked with `dump:"-"`. Uses field and type key. Indents with 2 spaces. Skips nil
// and zero values. Skips unexported fields.. Render empty slices as nil.
func Ast(v interface{}) string {
	b := Indenter{}
	b.AppendLine(fmt.Sprintf("%s:", marshalType(v)))
	marshalContainer(&b, v)
	return b.String()
}

func marshalType(v interface{}) string {
	return colorType(fmt.Sprintf("%T", v))
}

func marshalString(v interface{}) string {
	return colorString(fmt.Sprintf("%q", v))
}

// Dump contents of a either a slice or a struct.
func marshalContainer(b *Indenter, container interface{}) {
	containerType := reflect.TypeOf(container)
	switch containerType.Kind() {
	case reflect.Slice:
		for i := 0; i < reflect.ValueOf(container).Len(); i++ {
			item := reflect.ValueOf(container).Index(i).Interface()
			t, ok := item.(lexer.Token)
			if ok {
				b.AppendLine("- " + marshalToken(t))
				continue
			}
			// assume all elements are structs
			b.AppendLine(fmt.Sprintf("- %s:", marshalType(item)))
			b.Indent()
			marshalContainer(b, item)
			b.Dedent()
		}
		return
	case reflect.Struct:
		b.Indent()
		for i := 0; i < containerType.NumField(); i++ {
			field := containerType.Field(i)
			if field.PkgPath != "" {
				continue // skip unexported fields
			}
			// Skip fields marked with `dump:"-"`.
			tag := field.Tag.Get("dump")
			if tag == "-" {
				continue
			}

			item := reflect.ValueOf(container).Field(i).Interface()
			switch field.Type.Kind() {
			case reflect.Struct, reflect.Interface:
				if item == nil {
					continue
				}
				zeroer, ok := item.(zeroer)
				if ok && zeroer.IsZero() {
					continue
				}

				t, ok := item.(lexer.Token)
				if ok {
					if t.Type != lexer.Void {
						b.AppendLine(fmt.Sprintf("%s: %s", field.Name, marshalToken(t)))
					}
					continue
				}
				leaf, ok := item.(ast.Leaf)
				if ok {
					b.AppendLine(fmt.Sprintf("%s %s: %s", field.Name, marshalType(item), marshalToken(leaf.Token)))
				} else {
					b.AppendLine(fmt.Sprintf("%s %s:", field.Name, marshalType(item)))
					marshalContainer(b, item)

				}
			case reflect.Slice:
				if reflect.ValueOf(item).Len() == 0 {
					continue
				}

				// inline single item token slice.
				if reflect.ValueOf(item).Len() == 1 {
					item := reflect.ValueOf(item).Index(0).Interface()
					t, ok := item.(lexer.Token)
					if ok {
						b.AppendLine(fmt.Sprintf("%s: %s", field.Name, marshalToken(t)))
						continue
					}
				}
				b.AppendLine(fmt.Sprintf("%s:", field.Name))
				marshalContainer(b, item)
			case reflect.String:
				b.AppendLine(fmt.Sprintf("%s: %s", field.Name, marshalString(item)))
			default:
				b.AppendLine(fmt.Sprintf("%s: %s", field.Name, item))
			}
		}
		b.Dedent()
	default:
		panic(fmt.Errorf("unsupported type %T", container))
	}
}

func marshalToken(t lexer.Token) string {
	return fmt.Sprintf("%s %s", colorType(t.Type.String()), marshalString(t.Raw))
}

type zeroer interface {
	IsZero() bool
}
