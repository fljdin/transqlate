package render

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/dalibo/transqlate/lexer"
	"gitlab.com/dalibo/transqlate/rewrite"
)

type joinedErrors interface {
	Unwrap() []error
}

// CommentErrors renders errors as comments before erroring line.
func CommentErrors(tokens lexer.Tokener) (out lexer.Tokens) {
	var previous lexer.Token
	var line lexer.Tokens
	for _, t := range tokens.Tokens() {
		if len(line) > 0 && t.Line != line[0].Line {
			// Flush line.
			out = append(out, Renumber(previous, line)...)
			previous = out[len(out)-1]
			line = nil
		}

		line = append(line, t)
		// Add error as comment of first token in line.
		if t.Error != nil {
			var err rewrite.Err
			if errors.As(t.Error, &err) {
				t.Error = fmt.Errorf("TRANSLATION ERROR at +%d:%d: %w rule=%q", 1+t.Line, 1+t.Column, err, err.Rule)
			}
			line[0] = line[0].Comment(t.Error.Error())
		}
	}
	out = append(out, Renumber(previous, line)...)
	return
}

// NewErrorHinter returns a new error hinter.
//
// Use this to loop tokens and know whether they must be highlighter as error or not.
func NewErrorHinter(tokens lexer.Tokener) errorHint {
	return errorHint{input: tokens.Tokens()}
}

type errorHint struct {
	input    []lexer.Token
	erroring int // Number of tokens to highlight as error.
}

// Next returns the Next token and whether it should be highlighted as error.
//
// Returns void token at the end of the input. Forever.
func (e *errorHint) Next() (lexer.Token, bool) {
	if len(e.input) == 0 {
		return lexer.Token{}, false
	}

	next := e.input[0]
	e.input = e.input[1:]

	if next.Error != nil {
		count := 1
		var joined joinedErrors
		errs := []error{next.Error}
		if errors.As(next.Error, &joined) {
			errs = joined.Unwrap()
		}
		for _, err := range errs {
			var rerr rewrite.Err
			if !errors.As(err, &rerr) {
				continue
			}
			// Renumber filters void tokens.
			nodeCount := len(Renumber(lexer.Token{}, rerr.Node))
			// Highlight the longest node.
			if nodeCount > count {
				count = nodeCount
			}
		}
		if e.erroring < count {
			e.erroring = count
		}
	}
	erroring := e.erroring > 0
	if erroring {
		e.erroring--
	}
	return next, erroring
}

// Carret returns the SQL line of the token at pos with a carret under the token.
//
// Handles tabs as 4 spaces.
func Carret(line string, pos, width int) string {
	line = strings.TrimRight(line, "\n")

	var lastLine string
	lastLineBreak := strings.LastIndex(line, "\n")
	if lastLineBreak == -1 {
		lastLine = line
	} else {
		lastLine = line[lastLineBreak+1:]
	}
	caretPos := pos + 3*strings.Count(lastLine, "\t")

	b := strings.Builder{}
	b.WriteString(strings.ReplaceAll(line, "\t", "    "))
	b.WriteString("\n")
	b.WriteString(strings.Repeat(" ", caretPos))
	b.WriteString(strings.Repeat("^", width))
	return b.String()
}
