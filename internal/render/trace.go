package render

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/dalibo/transqlate/internal/parser"
)

func ParserTrace() {
	w := Indenter{}
	for _, event := range parser.Trace.Events() {
		if strings.HasPrefix(event, "leave:") {
			w.Dedent()
			continue
		}
		w.AppendLine(colorizeEvent(event))
		if strings.HasPrefix(event, "enter:") {
			w.Indent()
		}
	}
	os.Stderr.WriteString(w.String())
}

func colorizeEvent(s string) string {
	switch {
	case strings.HasPrefix(s, "enter: "):
		m := s[7:]
		space := strings.Index(m, " ")
		if space == -1 {
			return fmt.Sprintf("enter: %s", colorIdentifier(m))
		}
		return fmt.Sprintf("enter: %s%s", colorIdentifier(m[:space]), m[space:])
	case strings.HasPrefix(s, "consume: "):
		// consume: <type> <token>
		// See parser.state.consume
		m := s[9:]
		space := strings.Index(m, " ")
		return fmt.Sprintf("consume: %s%s", colorType(m[:space]), colorString(m[space:]))
	case strings.HasPrefix(s, "error: "):
		// error: <function> <type> <token>
		// See parser.state.errorf
		parts := strings.Split(s, " ")
		return fmt.Sprintf(
			"%s: %s: %s %s",
			colorError("error"),
			colorIdentifier(parts[1][:len(parts[1])-1]),
			colorType(parts[2]),
			colorString(parts[3]),
		)
	}
	return s
}
