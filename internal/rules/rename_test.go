package rules_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/internal/parser"
	"gitlab.com/dalibo/transqlate/internal/rules"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestRenameFunction(t *testing.T) {
	r := require.New(t)
	rule := rules.RenameFunction{From: "foo", To: "bar"}
	n := parser.MustParse("foo(1, 2)")
	r.True(rule.Match(n))
	n, err := rule.Rewrite(n)
	r.NoError(err)
	r.Equal("bar(1, 2)", lexer.Write(n))
}

func TestReplaceOperator(t *testing.T) {
	r := require.New(t)

	rule := rules.ReplaceOperator{
		From: []string{"MINUS"},
		To:   []string{"EXCEPT"},
	}
	n := parser.MustParse("SELECT 1 MINUS SELECT 2")
	r.True(rule.Match(n))
	n, err := rule.Rewrite(n)
	r.NoError(err)
	r.Equal("SELECT 1 EXCEPT SELECT 2", lexer.Write(n))
}

func TestReplaceOperatorMultiple(t *testing.T) {
	r := require.New(t)

	rule := rules.ReplaceOperator{
		From: []string{"MINUS"},
		To:   []string{"EXCEPT", "ALL"},
	}
	n := parser.MustParse("SELECT 1\nMINUS -- comment\nSELECT 2")
	r.True(rule.Match(n))
	n, err := rule.Rewrite(n)
	r.NoError(err)
	r.Equal("SELECT 1\nEXCEPT ALL -- comment\nSELECT 2", lexer.Write(n))
}
