// Package startwith converts Oracle recursive query
package startwith

import (
	"strings"

	"gitlab.com/dalibo/transqlate/internal/ast"
	"gitlab.com/dalibo/transqlate/internal/parser"
	"gitlab.com/dalibo/transqlate/lexer"
)

type Rule struct{}

func (Rule) String() string {
	return "rewrite START WITH as CTE"
}

func (Rule) Match(node ast.Node) bool {
	select_, _ := node.(ast.Select)
	return select_.Hierarchy != nil
}

func (Rule) Rewrite(node ast.Node) (ast.Node, error) {
	err := checkInput(node)
	if err != nil {
		return node, err
	}

	select_, startwith, connectby := splitHierarchy(node)
	prior := buildPrior(select_, startwith)
	recursion := buildRecursion(select_, connectby)
	cte := buildCTE(select_.ColumnNames(), prior, recursion)
	select_ = useCTE(select_, cte)

	return checkOutput(node, select_)
}

func splitHierarchy(node ast.Node) (select_ ast.Select, startwith ast.StartWith, connectby ast.ConnectBy) {
	select_ = node.(ast.Select)

	// copy tokens to remove prefix.
	select_.Select = append([]lexer.Token{}, select_.Select...)
	// ast.Space() will move SELECT prefix to WITH token.
	select_.Select[0].Prefix = ""

	// Handle START WITH before or after CONNECT BY.
	// The first clause wraps the last one.
	switch h := select_.Hierarchy.(type) {
	case ast.StartWith:
		startwith = h
		connectby = h.ConnectBy.(ast.ConnectBy)
		startwith.ConnectBy = nil
	case ast.ConnectBy:
		connectby = h
		startwith = h.StartWith.(ast.StartWith)
		connectby.StartWith = nil
	}
	select_.Hierarchy = nil
	return
}

// buildPrior build the first SELECT of the UNION.
//
// It's a copy of the original SELECT with the following changes:
//
//   - Add a WHERE clause with the START WITH condition
//   - Add a LEVEL column if not present
func buildPrior(select_ ast.Select, startwith ast.StartWith) ast.Select {
	prior := ast.Select{
		Select: append([]lexer.Token{}, select_.Select...),
		From:   ast.Copy(select_.From),
		Where: ast.Where{
			Keyword: lexer.MustLex("WHERE")[0],
		},
	}

	list := selectList(select_.List)
	for i, item := range list.Items {
		level, _ := item.Expression.(ast.Leaf)
		if strings.ToUpper(level.Token.Raw) == "LEVEL" {
			one := parser.MustParse("1")
			item.Expression = ast.SpaceFrom(ast.AliasTo(one, level.Token.Str), level)
		}
		list.Items[i] = item
	}
	prior.List = list

	prior.From.Tables.Items = prior.From.Tables.Items[:]
	prior.Where.And(startwith.Condition)
	prior.Where = ast.SpaceFrom(prior.Where, startwith)
	return prior
}

// buildRecursion build the second SELECT of the UNION.
//
// It's a copy of the original SELECT with the following changes:
//
//   - Hook the prior table by joining the CTE.
//   - Add a WHERE clause with the CONNECT BY condition
//   - Avoid column ambiguity by requalifying all columns.
//   - Add a LEVEL column if not present
func buildRecursion(select_ ast.Select, connectby ast.ConnectBy) ast.Select {
	recursion := ast.Select{
		Select: append([]lexer.Token{}, select_.Select...),
		From: ast.From{
			From: lexer.MustLex(" FROM")[0],
		},
	}
	list := selectList(select_.List)
	for i, item := range list.Items {
		level, _ := item.Expression.(ast.Leaf)
		if strings.ToUpper(level.Token.Raw) == "LEVEL" {
			tableExpr := parser.MustParse(`"prior".%s + 1`, level.Token)
			item.Expression = ast.SpaceFrom(ast.AliasTo(tableExpr, level.Token.Str), item.Expression)
		} else {
			item.Expression = requalifyRecursionColumns(item.Expression, parser.MustParse(`"recursion"`))
		}
		list.Items[i] = item
	}
	recursion.List = list

	tableExpr := parser.MustParse(" %s", select_.From.Names()[0])
	recursion.From.Append(ast.AliasTo(tableExpr, "recursion"))
	on := ast.Where{
		Keyword: lexer.MustLex(" ON")[0],
	}
	on.And(requalifyConnectBy(connectby.Condition))
	on = ast.SpaceFrom(on, connectby)
	// Final NL is preserved by AliasTo
	hierarchy := parser.MustParse(" hierarchy\n")
	join := ast.Join{
		Join:      lexer.MustLex("JOIN"),
		Right:     ast.AliasTo(hierarchy, "prior"),
		Condition: ast.EndLine(on),
	}
	recursion.From.Append(join)
	return recursion
}

// requalifyConnectBy prefixes columns in a CONNECT BY condition.
//
// It's used to avoid ambiguity between the prior and recursion tables. Columns prefixed with PRIOR
// will be prefixed with "prior". and other columns with "recursion.".
func requalifyConnectBy(n ast.Node) ast.Node {
	return n.Visit(func(n ast.Node) ast.Node {
		infix, _ := n.(ast.Infix)
		if !infix.Is("=") {
			return n
		}
		prior := parser.MustParse(`"prior"`)
		recursion := parser.MustParse(`recursion`)
		var leftPrior bool
		infix.Left, leftPrior = requalifyPriorColumns(infix.Left, prior)
		if leftPrior {
			infix.Right = requalifyRecursionColumns(infix.Right, recursion)
		} else {
			infix.Left = requalifyRecursionColumns(infix.Left, recursion)
			infix.Right, _ = requalifyPriorColumns(infix.Right, prior)
		}
		return infix
	})
}

// buildCTE returns the "hierarchy" buildCTE.
func buildCTE(columns []string, prior, recursion ast.Select) ast.CTE {
	cte := ast.CTE{
		Name: lexer.MustLex(" hierarchy")[0],
		Columns: ast.Columns{
			Open:  lexer.MustLex("(")[0],
			Close: lexer.MustLex(")")[0],
		},
		As:   lexer.MustLex(" AS")[0],
		Open: lexer.MustLex(" (\n")[0],
		Query: ast.Infix{
			Left:  prior,
			Op:    lexer.MustLex("UNION ALL\n"),
			Right: recursion,
		},
		Close: lexer.MustLex(")\n")[0],
	}

	// Build signature
	columnNames := columns
	l := len(columnNames)
	for i, name := range columnNames {
		item := ast.Column{
			Name: lexer.MustLex("%s", name)[0],
		}
		if i > 0 {
			item = ast.OneSpace(item)
		}
		if i < l-1 {
			item.Comma = lexer.MustLex(",")[0]
		}
		cte.Columns.Names = append(cte.Columns.Names, item)
	}
	return cte
}

// useCTE attaches the WITH statement and rebuilds the original SELECT.
func useCTE(select_ ast.Select, cte ast.CTE) ast.Select {
	tokens := lexer.MustLex("WITH RECURSIVE")
	// RECURSIVE is lexed as Identifier. Force Keyword like the parse would do.
	tokens[1].Type = lexer.Keyword
	select_.With = ast.With{
		With:      tokens[0],
		Recursive: tokens[1],
		CTEs:      []ast.CTE{cte},
	}
	// Remove keywords after SELECT
	select_.Select = select_.Select[:1]
	// select_.List is preserved as is.
	// Recreate FROM clause with CTE
	tableName := select_.From.Names()[0]
	select_.From = ast.From{From: lexer.MustLex(" %s", select_.From.From)[0]}
	hierarchy := parser.MustParse(" hierarchy\n")
	select_.From.Tables.Append(ast.AliasTo(hierarchy, tableName))
	return select_
}

func selectList(n ast.Node) ast.Grouping {
	list, ok := n.(ast.Grouping)
	if ok {
		list = ast.Copy(list)
	} else {
		// Transform scalar expression into a list.
		list.Items = []ast.Postfix{{Expression: n}}
	}
	return list
}
