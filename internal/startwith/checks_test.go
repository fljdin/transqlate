package startwith_test

import (
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/oracle"
)

func TestNoCyle(t *testing.T) {
	r := require.New(t)
	in := dedent.Dedent(`
	SELECT col
	FROM t
	START WITH col IS NULL
	CONNECT BY NOCYCLE PRIOR col = col`)
	out, err := oracle.Translate("<test>", in)
	r.Error(err)
	r.Equal(in, out)
}

func TestPath(t *testing.T) {
	r := require.New(t)
	in := dedent.Dedent(`
	SELECT col, SYS_CONNECT_BY_PATH(col, '/') AS path
	FROM t
	START WITH col IS NULL
	CONNECT BY PRIOR col = col`)
	out, err := oracle.Translate("<test>", in)
	r.Error(err)
	r.ErrorContains(err, "SYS_CONNECT_BY_PATH")
	r.Equal(in, out)
}
