package startwith

import (
	"gitlab.com/dalibo/transqlate/internal/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

func requalifyPriorColumns(n ast.Node, prior ast.Node) (ast.Node, bool) {
	modified := false
	n = n.Visit(func(n ast.Node) ast.Node {
		prefix, ok := n.(ast.Prefix)
		if !ok {
			return n
		}
		if !prefix.Is("PRIOR") {
			return n
		}
		modified = true
		return ast.TransformColumnRef(prefix.Expression, func(n ast.Node) ast.Node {
			return setColumnRefTable(n, prior)
		})
	})
	return n, modified
}

func requalifyRecursionColumns(n ast.Node, recursion ast.Node) ast.Node {
	return ast.TransformColumnRef(n, func(n ast.Node) ast.Node {
		return setColumnRefTable(n, recursion)
	})
}

func setColumnRefTable(n ast.Node, table ast.Node) ast.Node {
	switch col := n.(type) {
	case ast.Leaf:
		return ast.SpaceFrom(ast.Infix{
			Left:  table,
			Op:    lexer.MustLex("."),
			Right: ast.SpaceFrom(col, table),
		}, n)
	case ast.Infix:
		col.Left = table
		return ast.SpaceFrom(col, n)
	}
	panic("unhandled column ref type")
}
