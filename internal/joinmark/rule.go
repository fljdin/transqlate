// Package joinmark converts Oracle JOIN mark
package joinmark

import (
	"log/slog"

	"gitlab.com/dalibo/transqlate/internal/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

type Rule struct{}

func (Rule) String() string {
	return "replace (+) with OUTER JOIN"
}

func (Rule) Match(n ast.Node) bool {
	s, _ := n.(ast.Select)
	return hasJoinMark(s.Where)
}

// Rewrite replaces join mark with OUTER JOIN.
//
// Since [rewrite.Engine] uses depth-first walk, all sub-queries are already translated.
// This rule can safely recurse to handle all join marks in the tree.
//
// The rule is not perfect and may fail to rewrite some queries.
// It will fail if:
//
//   - the SELECT * order is changed.
//   - a column in WHERE is not prefixed by a table.
//   - the rule loses table in FROM clause. (it's a bug)
//   - the rule loses condition in WHERE and JOIN. (also a bug)
func (r Rule) Rewrite(node ast.Node) (ast.Node, error) {
	select_ := node.(ast.Select)
	where := select_.Where
	origFrom := select_.From
	tableOrder := origFrom.Names()
	err := checkOriginal(tableOrder, where)
	if err != nil {
		return node, err
	}

	keepOrder := select_.IsStar()
	slog.Debug("Found tables.", "tables", tableOrder, "ordered", keepOrder)

	newFrom := origFrom        // Copy
	newFrom.Tables.Items = nil // reset tables.

	slog.Debug("JOIN root.", "table", tableOrder[0])
	newFrom.Append(ast.SpaceFrom(origFrom.Tables.Items[0].Expression, origFrom.Tables.Items[0]))

	for _, item := range origFrom.Tables.Items[1:] {
		rightExpr := ast.SpaceFrom(item.Expression, item)
		right := ast.TableName(rightExpr)

		var whereWithRight ast.Where
		var leftTables []string
		whereWithRight, leftTables, where = extractJoinsWith(origFrom, where, newFrom.Names(), right)
		if len(whereWithRight.Conditions) == 0 { // right is not joined.
			newFrom.Append(rightExpr)
			continue
		}

		for _, left := range leftTables {
			newFrom, whereWithRight = processJoin(keepOrder, origFrom, newFrom, left, right, whereWithRight)
		}

		// Some conditions on right may be postponed, for reordering. Put back unprocessed conditions.
		where.Append(whereWithRight.Conditions...)
	}

	// Rewrite the SELECT.
	select_.From = newFrom
	if len(where.Conditions) == 0 {
		select_.Where = ast.Where{}
	} else {
		select_.Where = where
	}
	select_ = spaceSelect(select_)

	return checkRewrite(node, select_, keepOrder)
}

// processJoin builds a single join out of join marks.
//
// This is where the heavy duty is done.
//
// Receives a mixed information of the original query, the remaining where clause related to a
// table, the current state of rewritten FROM.
//
// It splits whereWithRight as ON and trailing WHERE condition for other joins on right table.
//
// Eventually, right and left are swapped. Either to prefer a LEFT JOIN over a RIGHT JOIN, or to
// satisfy tables dependencies since JOIN are ordered unlike join mark.
func processJoin(keepOrder bool, origFrom, newFrom ast.From, left, right string, whereWithRight ast.Where) (ast.From, ast.Where) {
	if !newFrom.Contains(left) {
		left, right = right, left
		// checkRewrite will raise an error if SELECT *.
		slog.Debug("Reordering tables for join.", "left", left, "right", right)
	}

	on, whereWithRight := splitWhereOn(whereWithRight, left, right)

	direction := guessJoinDirection(right, on)
	if direction == "RIGHT" && !keepOrder {
		// Replace left by right in the processed from list.
		leftExpr := origFrom.Tables.Items[origFrom.Index(right)].Expression
		if newFrom.IsJoin(left) {
			slog.Debug("left is already joined, don't prefer right join.", "left", left)
		} else {
			newFrom.Replace(left, leftExpr)
			slog.Debug("Rewrite RIGHT as LEFT join.", "left", left, "direction", direction, "right", right)
			left, right = right, left
			direction = "LEFT"
		}
	}
	rightExpr := origFrom.Tables.Items[origFrom.Index(right)].Expression
	slog.Debug("JOIN", "left", left, "direction", direction, "right", right, "on", lexer.Write(on))
	newFrom.Append(buildJoin(direction, rightExpr, on))

	return newFrom, whereWithRight
}

func spaceSelect(s ast.Select) ast.Select {
	if s.Where.Keyword.Prefix == "" {
		s.From = ast.EndLine(s.From)
	}
	s = ast.TrimNewLine(s)
	return s
}
