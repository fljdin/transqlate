package joinmark

import (
	"log/slog"
	"slices"

	mapset "github.com/deckarep/golang-set/v2"
	"gitlab.com/dalibo/transqlate/internal/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

// extractJoinsWith searches for joins on one table
//
// Returns all conditions related to the table and the list of joined tables. The result must be
// processed as one join per table. Only left tables are returned. The right tables is postponed for
// next iteration in the FROM list.
func extractJoinsWith(origFrom ast.From, where ast.Where, left []string, right string) (ast.Where, []string, ast.Where) {
	// First, gather **all** join marked condition for the current table.
	whereWithTable, whereWithoutTable := splitWhereWithTable(where, left, right)
	if len(whereWithTable.Conditions) == 0 {
		slog.Debug("Not joined.", "table", right)
		return ast.Where{}, nil, where
	}

	// Determine the number of joined tables.
	joinedTables := origFrom.Sort(listAllReferencedTables(whereWithTable)...)
	if len(joinedTables) == 1 {
		slog.Debug("Can't find left table. Ambiguous column?", "right", right, "on", lexer.Write(whereWithTable))
		// checkRewrite will raise on join mark left in where.
		return ast.Where{}, nil, where
	}
	// Postpone right tables for next iterations.
	leftTables := joinedTables[:slices.Index(joinedTables, right)]
	if len(leftTables) == 0 {
		slog.Debug("Joins next table. Postpone.")
		return ast.Where{}, nil, where
	}
	return whereWithTable, leftTables, whereWithoutTable
}

// splitWhereWithTable returns conditions with expression referencing only the table.
//
// It is used to find whether a table is joined with another. Thus, it matches only conditions where
// at least one side of the condition is marked with joinmark.
func splitWhereWithTable(where ast.Where, left []string, table string) (ast.Where, ast.Where) {
	if where.IsZero() {
		return where, where
	}
	slog.Debug("Search joinmark conditions.", "table", table, "expr", lexer.Write(where))
	whereWithTable := ast.Where{Keyword: where.Keyword}
	whereWithoutTable := ast.Where{Keyword: where.Keyword}
	for _, item := range where.Conditions {
		referencedTables := listReferencedTables(item.Expression)
		// Extract referenced tables not already joined.
		var newTables []string
		for _, t := range referencedTables {
			if !slices.Contains(left, t) {
				newTables = append(newTables, t)
			}
		}
		if len(newTables) > 0 && !slices.Contains(newTables, table) {
			// Exclude condition without the right table.
			whereWithoutTable.Append(item)
			continue
		}
		if !hasJoinMark(item.Expression) {
			// Exclude condition without join mark.
			whereWithoutTable.Append(item)
			continue
		}

		slog.Debug("Found condition for join on", "table", table, "expr", lexer.Write(item.Expression))
		whereWithTable.Append(item)
	}
	return whereWithTable, whereWithoutTable
}

// splitWhereOn returns conditions with only expression referencing the left or right table.
//
// conditions is the subset of WHERE referencing either left or right. Return two conditions subset:
// the one refering to both left and right or only one of those. other is the rest.
//
// e.g. a.col = b.id AND a.col > 2000 AND b.col = c.id
// Will be split in:
// on: a.col = b.id AND a.col > 2000
// other: b.col = c.id
func splitWhereOn(where ast.Where, left, right string) (on ast.Where, other ast.Where) {
	if where.IsZero() {
		panic("wrong join condition")
	}
	referencedTables := listAllReferencedTables(where)
	if len(referencedTables) == 2 {
		on = where // Fast path for WHERE with a single join.
		on.Keyword.Set("ON")
		return
	}

	on = ast.Where{Keyword: lexer.MustLex(" ON")[0]}
	other = ast.Where{Keyword: where.Keyword} // Starts a new WHERE
	allowed := mapset.NewSet(left, right)
	for _, item := range where.Conditions {
		referencedTables := listReferencedTables(item.Expression)
		if !allowed.Contains(referencedTables...) {
			other.Append(item)
			continue
		}
		if !hasJoinMark(item.Expression) {
			// Exclude condition without join mark.
			other.Append(item)
			continue
		}
		slog.Debug("Found condition for join.", "expr", lexer.Write(item.Expression))
		on.Append(item)
	}
	return
}
