package joinmark

// This file contains code to browse join condition in a WHERE.

import (
	mapset "github.com/deckarep/golang-set/v2"
	"gitlab.com/dalibo/transqlate/internal/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

// listAllReferencedTables returns table names referenced in an expression.
//
// This is used to find tables in WHERE or ON clauses.
//
// e.g. WHERE a.col = b.col(+) -> [a, b]
//
// The support for syntax is limited. Accepts references in operation and function call only.
func listAllReferencedTables(expr ast.Node) []string {
	if expr == nil {
		return nil
	}

	tables := mapset.NewSet[string]()
	expr.Visit(func(n ast.Node) ast.Node {
		switch n.(type) {
		case ast.Postfix, ast.Infix, ast.Call:
			tables.Append(listReferencedTables(n)...)
		}
		return n
	})
	return tables.ToSlice()
}

// listReferencedTables returns table names referenced in a condition.
//
// This is used to find tables in WHERE or ON clauses. Recursion is limited to the first level of
// some expressions.
//
// expr is supposed to match isCondition(). e.g.
//
// e.g. t1.col = t2.col(+) -> [t1, t2]
// e.g. t1.col > 2000 -> [t1]
// e.g. NOT t1.col(+) -> [t1]
//
// Unsupported conditions are not migrated. checkRewrite will detect this and raise
// an error to user.
func listReferencedTables(expr ast.Node) (names []string) {
	switch expr := expr.(type) {
	case ast.Leaf:
		if expr.Token.Type == lexer.Identifier {
			// table less column reference.
			// e.g. user_id(+)
			names = append(names, "")
		}
	case ast.Call:
		for _, arg := range expr.Args.Items {
			names = append(names, listReferencedTables(arg.Expression)...)
		}
	case ast.Prefix:
		names = append(names, listReferencedTables(expr.Expression)...)
	case ast.Infix:
		if expr.Is("AND") || expr.Is("OR") {
			break // AND is rather a combinator of conditions rather than a condition.
		}

		if expr.Is(".") {
			left, _ := expr.Left.(ast.Leaf)
			if left.Token.Type == lexer.Identifier {
				names = append(names, left.Token.Str)
			}
		} else {
			names = append(names, listReferencedTables(expr.Left)...)
			names = append(names, listReferencedTables(expr.Right)...)
		}

	case ast.Postfix:
		names = append(names, listReferencedTables(expr.Expression)...)
	}
	return
}

// listMarkedTables returns referenced tables with join mark only.
//
// It is used to determine which table of a condition is the outer table.
func listMarkedTables(expr ast.Node) (marked []string) {
	expr.Visit(func(n ast.Node) ast.Node {
		postfix, _ := n.(ast.Postfix)
		if !postfix.Is("(+)") {
			return n
		}
		marked = append(marked, listReferencedTables(postfix.Expression)...)
		return n
	})
	return
}
