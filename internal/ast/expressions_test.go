package ast_test

import (
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/internal/ast"
	"gitlab.com/dalibo/transqlate/lexer"
	"gitlab.com/dalibo/transqlate/internal/parser"
)

func TestAlias(t *testing.T) {
	r := require.New(t)
	a := ast.Alias{
		Expression: ast.Leaf{Token: lexer.MustLex("foo")[0]},
		As:         lexer.MustLex(" AS")[0],
		Name:       lexer.MustLex(" a")[0],
	}
	r.Equal("foo AS a", lexer.Write(a))

	a = ast.Alias{
		Expression: ast.Leaf{Token: lexer.MustLex("foo")[0]},
		Name:       lexer.MustLex(" a")[0],
	}
	r.Equal("foo a", lexer.Write(a))
}

func TestGroupingEdit(t *testing.T) {
	r := require.New(t)
	g := parser.MustParse(dedent.Dedent(`
	(
	-- before
	1 -- after
	)
	`)).(ast.Grouping)

	g.Append(parser.MustParse(dedent.Dedent(`
	-- before 2
	2 -- after 2
	`)[1:]))

	// Put comma before comment.
	r.Equal(dedent.Dedent(`
	(
	-- before
	1, -- after
	-- before 2
	2 -- after 2
	)
	`), lexer.Write(g))

	// Put comma before comment.
	g.Insert(0, parser.MustParse(dedent.Dedent(`
	-- before 0
	0 -- after 0
	`)[1:]))
	r.Equal(dedent.Dedent(`
	(
	-- before 0
	0, -- after 0
	-- before
	1, -- after
	-- before 2
	2 -- after 2
	)
	`), lexer.Write(g))

	// Remove trailing comma before comment.
	g.Delete(2)
	r.Equal(dedent.Dedent(`
	(
	-- before 0
	0, -- after 0
	-- before
	1 -- after
	)
	`), lexer.Write(g))
}

func TestCallIs(t *testing.T) {
	r := require.New(t)

	// Function node is a Leaf.
	c := parser.MustParse("foo()").(ast.Call)
	r.True(c.Is("foo"))
	r.False(c.Is("foo", "baz"))

	// Function node is an Infix.
	c = parser.MustParse("foo.bar()").(ast.Call)
	r.True(c.Is("foo", "bar"))
	r.False(c.Is("foo"))
	r.False(c.Is("bar"))

	// Function is an unhandled type.
	// Here, an unexpected Call.
	c = parser.MustParse("foo()()").(ast.Call)
	r.False(c.Is("foo"))
}
