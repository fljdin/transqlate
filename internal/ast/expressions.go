package ast

import (
	"fmt"
	"strings"

	"gitlab.com/dalibo/transqlate/lexer"
)

// Alias renames an expression with or without AS keyword.
type Alias struct {
	Expression Node
	// As may be Void if AS keyword is omitted.
	As lexer.Token
	// Name is the new name, an identifier.
	Name lexer.Token
}

func (a Alias) String() string {
	return a.Name.Str
}

func (a Alias) Tokens() []lexer.Token {
	t := a.Expression.Tokens()
	t = append(t, a.As)
	return append(t, a.Name)
}

func (a Alias) TransformStart(f Transformer[lexer.Token]) Node {
	a.Expression = a.Expression.TransformStart(f)
	return a
}

func (a Alias) TransformEnd(f Transformer[lexer.Token]) Node {
	a.Name = f(a.Name)
	return a
}

func (a Alias) Visit(f Transformer[Node]) Node {
	a.Expression = a.Expression.Visit(f)
	if a.Expression == nil {
		return nil
	}
	return f(a)
}

func (a Alias) indent(indentation string) Node {
	a.Expression = Indent(a.Expression, indentation)
	return a
}

// AliasTo ensure expr has a name.
//
// If expr is an alias, it renames it.
// If expr is not an alias, it creates a new alias with the given name.
func AliasTo(expr Node, name string) Alias {
	alias, ok := expr.(Alias)
	if ok {
		alias.Name.Set(name)
		return alias
	}
	alias.Expression = expr
	tokens := lexer.MustLex(` AS "%s"`, name)
	alias.As = tokens[0]
	alias.Name = tokens[1]
	alias, alias.Expression = SwapWhitespaces(alias, expr)
	return alias
}

// Between holds BETWEEN or NOT BETWEEN expression.
type Between struct {
	Expression Node
	Between    []lexer.Token
	Start      Node
	And        lexer.Token
	End        Node
}

func (b Between) Tokens() []lexer.Token {
	t := b.Expression.Tokens()
	t = append(t, b.Between...)
	t = append(t, b.Start.Tokens()...)
	t = append(t, b.And)
	t = append(t, b.End.Tokens()...)
	return t
}

func (b Between) TransformStart(f Transformer[lexer.Token]) Node {
	b.Expression = b.Expression.TransformStart(f)
	return b
}

func (b Between) TransformEnd(f Transformer[lexer.Token]) Node {
	b.End = b.End.TransformEnd(f)
	return b
}

func (b Between) Visit(f Transformer[Node]) Node {
	b.Expression = b.Expression.Visit(f)
	if b.Expression == nil {
		return nil
	}
	b.Start = b.Start.Visit(f)
	if b.Start == nil {
		return nil
	}
	b.End = b.End.Visit(f)
	if b.End == nil {
		return nil
	}
	return f(b)
}

// Call holds a function call.
type Call struct {
	Function Node     // Identifier or qualified name
	Args     Grouping // The grouping containing arguments
}

func (c Call) Tokens() []lexer.Token {
	t := []lexer.Token{}
	if c.Function != nil {
		t = append(t, c.Function.Tokens()...)
	}
	t = append(t, c.Args.Tokens()...)
	return t
}

func (c Call) TransformStart(f Transformer[lexer.Token]) Node {
	c.Function = c.Function.TransformStart(f)
	return c
}

func (c Call) TransformEnd(f Transformer[lexer.Token]) Node {
	c.Args = c.Args.TransformEnd(f).(Grouping)
	return c
}

func (c Call) Visit(f Transformer[Node]) Node {
	c.Function = c.Function.Visit(f)
	args := c.Args.Visit(f)
	if args == nil {
		// Keep parenthesis. Just remove args.
		c.Args.Items = nil
	} else {
		c.Args = args.(Grouping)
	}
	return f(c)
}

// Is reports whether the function name matches the given strings.
//
// Accepts one or two strings. If two strings are given, the first one is the
// package/schema name and the second one is the function name.
func (c Call) Is(s ...string) bool {
	if len(s) == 1 {
		leaf, _ := c.Function.(Leaf)
		return leaf.In(s[0])
	}

	if len(s) == 2 {
		infix, _ := c.Function.(Infix)
		if !infix.Is(".") {
			return false
		}
		left, right := lexer.Edges(infix)
		return left.Str == s[0] && right.Str == s[1]
	}

	panic("accepts one or two arguments")
}

// In reports whether function name is in the given list of strings.
func (c Call) In(s ...string) bool {
	for _, v := range s {
		if c.Is(v) {
			return true
		}
	}
	return false
}

func (c Call) indent(indentation string) Node {
	c.Function = Indent(c.Function, indentation)
	_, before := lexer.Edges(c.Function)
	// Move indentation after function name.
	indentation = lexer.IncreaseIndentation(indentation, lexer.Write(c.Function))
	// Increase pad a space after function name. Remove it.
	indentation = indentation[1:]
	c.Args, _ = IndentAfter(before, c.Args, indentation)
	// Remove space between function and args.
	c.Args = c.Args.TransformStart(func(t lexer.Token) lexer.Token {
		if t.LeadingComment() != "" {
			return t
		}
		t.Prefix = ""
		return t
	}).(Grouping)
	return c
}

// Grouping holds comma separated expressions, eventually braced by parenthesis.
//
// e.g. a.id, a.name or (1, 2) or [3, 4]
//
// Open and Close can be Void for SELECT list, FROM clause.
// All Items are Postfix node with separator as postfix operator.
// Last Item has void operator because SQL does not allow trailing comma.
//
// Used for function call arguments, sub-queries, function signature, etc.
type Grouping struct {
	Open  lexer.Token
	Items []Postfix
	Close lexer.Token
}

func (g Grouping) Tokens() []lexer.Token {
	t := []lexer.Token{}
	t = append(t, g.Open)
	for _, i := range g.Items {
		t = append(t, i.Tokens()...)
	}
	t = append(t, g.Close)
	return t
}

func (g Grouping) TransformStart(f Transformer[lexer.Token]) Node {
	if g.Open.IsZero() {
		if len(g.Items) > 0 {
			g.Items = append([]Postfix{}, g.Items...) // copy
			g.Items[0] = g.Items[0].TransformStart(f).(Postfix)
		}
		return g
	}
	g.Open = f(g.Open)
	return g
}

func (g Grouping) TransformEnd(f Transformer[lexer.Token]) Node {
	if g.Close.IsZero() {
		if len(g.Items) > 0 {
			g.Items = append([]Postfix{}, g.Items...) // copy
			last := len(g.Items) - 1
			g.Items[last] = g.Items[last].TransformEnd(f).(Postfix)
		}
		return g
	}
	g.Close = f(g.Close)
	return g
}

func (g Grouping) Visit(f Transformer[Node]) Node {
	originalItems := g.Items
	g.Items = nil // Reset items.
	for _, i := range originalItems {
		n := i.Visit(f)
		if n == nil {
			continue // Skip nil items.
		}
		g.Items = append(g.Items, n.(Postfix))
	}
	if len(g.Items) == 0 {
		// Drop empty grouping.
		return nil
	}
	return f(g)
}

func (g Grouping) indent(indentation string) Node {
	if !g.Open.IsZero() {
		g.Open = g.Open.Indent(indentation)
		if g.Open.Suffix == "\n" {
			g.Open.Suffix = ""
		}
	}
	before := g.Open
	itemIndentation := indentation + strings.Repeat(" ", len(g.Open.Raw))
	for i, item := range g.Items {
		item, before = IndentAfter(before, item, itemIndentation)
		g.Items[i] = item
	}
	if !g.Close.IsZero() && before.EndsLine() {
		g.Close = g.Close.Indent(indentation)
	}

	return g
}

// Append adds a new item to the grouping.
//
// Ensure a comma between items.
func (g *Grouping) Append(n Node) {
	if l := len(g.Items); l > 0 {
		end := l - 1
		g.Items[end] = g.Items[end].SetOperator(lexer.MustLex(",")[0])
	}
	g.Items = append(g.Items, Postfix{Expression: n})
}

// Delete removes an item at position.
func (g *Grouping) Delete(pos int) (expr Node) {
	l := len(g.Items)
	if pos < 0 || l <= pos {
		panic(fmt.Sprintf("position %d out of range", pos))
	}
	expr = SpaceFrom(g.Items[pos].Expression, g.Items[pos])
	g.Items = append(g.Items[:pos], g.Items[pos+1:]...)
	if pos == l-1 {
		end := l - 2
		// Remove trailing comma.
		g.Items[end] = g.Items[end].RemoveOperator()
	}
	return
}

// Insert adds a new item to the grouping at the given position.
//
// Panics if position is out of range.
func (g *Grouping) Insert(pos int, n Node) {
	if pos < 0 || len(g.Items) < pos {
		panic(fmt.Sprintf("position %d out of range", pos))
	}

	i := Postfix{Expression: n}
	comma := lexer.MustLex(",")[0]

	if pos != len(g.Items) {
		i = i.SetOperator(comma)
	}

	g.Items = append(g.Items[:pos], append([]Postfix{i}, g.Items[pos:]...)...)
}
