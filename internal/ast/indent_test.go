package ast_test

import (
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/internal/ast"
	"gitlab.com/dalibo/transqlate/lexer"
	"gitlab.com/dalibo/transqlate/internal/parser"
)

func TestIndentAfter(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("1 + 2")

	before := lexer.MustLex("FROM")[0]
	n2, _ := ast.IndentAfter(before, n, "\t")
	r.Equal(" 1 + 2", lexer.Write(n2))

	before = lexer.MustLex("(")[0]
	n2, _ = ast.IndentAfter(before, n, "\t")
	r.Equal("1 + 2", lexer.Write(n2))

	before = lexer.MustLex(".")[0]
	n2, _ = ast.IndentAfter(before, n, "\t")
	r.Equal("1 + 2", lexer.Write(n2))

	before = lexer.MustLex("FROM\n")[0]
	n2, _ = ast.IndentAfter(before, n, "\t")
	r.Equal("\t1 + 2", lexer.Write(n2))
}

func TestIndentSelect(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse(dedent.Dedent(`
	SELECT id, name, manager
	FROM employees
	WHERE manager IS NULL
	`)[1:])
	r.Equal(""+ // for gofmt to align following strings
		"SELECT id, name, manager\n"+
		"  FROM employees\n"+
		" WHERE manager IS NULL\n",
		lexer.Write(ast.Indent(n, "")))
}

func TestIndentSelectListFrom(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse(dedent.Dedent(`
	SELECT id,
	name,
	manager
	FROM employees,
	managers
	`)[1:])
	r.Equal(dedent.Dedent(`
	SELECT id,
	       name,
	       manager
	  FROM employees,
	       managers
	`)[1:], lexer.Write(ast.Indent(n, "")))
}

func TestIndentWhere(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse(dedent.Dedent(`
	SELECT * FROM employees
	WHERE manager IS NULL
	AND name = 'Alice'
	`)[1:])
	r.Equal(""+ // for gofmt to align following strings
		"SELECT * FROM employees\n"+
		" WHERE manager IS NULL\n"+
		"   AND name = 'Alice'\n",
		lexer.Write(ast.Indent(n, "")))
}

func TestIndentCase(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse(dedent.Dedent(`
	CASE manager WHEN NULL THEN 'no manager' ELSE 'other' END
	`)[1:])
	r.Equal(dedent.Dedent(`
	CASE manager WHEN NULL THEN 'no manager' ELSE 'other' END
	`)[1:], lexer.Write(ast.Indent(n, "")))

	n = parser.MustParse(dedent.Dedent(`
	CASE
	WHEN manager IS NULL THEN 'no manager'
	WHEN name = 'Alice'
	THEN 'Alice'
	ELSE 'other'
	END
	`)[1:])
	r.Equal(dedent.Dedent(`
	CASE WHEN manager IS NULL THEN 'no manager'
	     WHEN name = 'Alice'
	     THEN 'Alice'
	     ELSE 'other'
	END
	`)[1:], lexer.Write(ast.Indent(n, "")))

	n = parser.MustParse(dedent.Dedent(`
	CASE manager
	WHEN NULL THEN fn(1,
	2)
	WHEN fn(1,
	2)
	THEN fn(1,
	2)
	ELSE 1
	AND 2
	END
	`)[1:])
	r.Equal(dedent.Dedent(`
	CASE manager
	WHEN NULL THEN fn(1,
	                  2)
	WHEN fn(1,
	        2)
	THEN fn(1,
	        2)
	ELSE 1
	 AND 2
	END
	`)[1:], lexer.Write(ast.Indent(n, "")))
}

func TestIndentCall(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse(dedent.Dedent(`
	SELECT my(name,
	'foo')
	`)[1:])
	r.Equal(dedent.Dedent(`
	SELECT my(name,
	          'foo')
	`)[1:], lexer.Write(ast.Indent(n, "")))
}
