package ast

import (
	"strings"

	"gitlab.com/dalibo/transqlate/lexer"
)

// Case is a SQL CASE statement.
type Case struct {
	Case       lexer.Token
	Expression Node // nil if CASE is used for simple WHEN/THEN.
	Cases      []When
	Else       Prefix // Use Prefix.IsZero() to check for presence of ELSE.
	End        lexer.Token
}

func (c Case) Tokens() []lexer.Token {
	t := []lexer.Token{c.Case}
	if c.Expression != nil {
		t = append(t, c.Expression.Tokens()...)
	}
	for _, w := range c.Cases {
		t = append(t, w.Tokens()...)
	}
	t = append(t, c.Else.Tokens()...)
	return append(t, c.End)
}

func (c Case) TransformStart(f Transformer[lexer.Token]) Node {
	c.Case = f(c.Case)
	return c
}

func (c Case) TransformEnd(f Transformer[lexer.Token]) Node {
	c.End = f(c.End)
	return c
}

func (c Case) Visit(f Transformer[Node]) Node {
	if c.Expression != nil {
		c.Expression = c.Expression.Visit(f)
	}
	var whens []When
	for _, w := range c.Cases {
		n := w.Visit(f)
		if n != nil {
			whens = append(whens, n.(When))
		}
	}
	c.Cases = whens
	if c.Else.Token.Type != lexer.Void {
		c.Else, _ = c.Else.Visit(f).(Prefix)
	}
	return f(c)
}

func (c Case) indent(indentation string) Node {
	c.Case = c.Case.Indent(indentation)
	before := c.Case
	itemIndent := indentation + strings.Repeat(" ", len(c.Case.Raw)+1)
	if c.Expression == nil {
		if before.EndsLine() {
			// Put when right after CASE.
			c.Case = c.Case.TrimNewLine()
			before = c.Case
		}
		// WHEN follows CASE, push the river after WHEN
		itemIndent += strings.Repeat(" ", len("WHEN "))
	} else {
		c.Expression, before = IndentAfter(before, c.Expression, itemIndent)
	}
	for i, w := range c.Cases {
		c.Cases[i], before = IndentAfter(before, w, itemIndent)
	}
	if c.Else.Token.Type != lexer.Void {
		c.Else, before = IndentAfter(before, c.Else, itemIndent)
	}
	if before.EndsLine() {
		c.End = c.End.Indent(indentation)
	}
	return c
}

// When is the WHEN-THEN clause of a CASE statement.
type When struct {
	When Prefix
	Then Prefix
}

func (w When) Tokens() []lexer.Token {
	return append(w.When.Tokens(), w.Then.Tokens()...)
}

func (w When) TransformStart(f Transformer[lexer.Token]) Node {
	w.When = w.When.TransformStart(f).(Prefix)
	return w
}

func (w When) TransformEnd(f Transformer[lexer.Token]) Node {
	w.Then = w.Then.TransformEnd(f).(Prefix)
	return w
}

func (w When) Visit(f Transformer[Node]) Node {
	w.When = w.When.Visit(f).(Prefix)
	w.Then = w.Then.Visit(f).(Prefix)
	return f(w)
}

func (w When) indent(indentation string) Node {
	// Assume river after WHEN.
	w.When = Indent(w.When, indentation)
	_, before := lexer.Edges(w.When)
	if !before.EndsLine() {
		// Push river after WHEN expression.
		indentation = lexer.IncreaseIndentation(indentation, lexer.Write(w.When))
	}
	w.Then, _ = IndentAfter(before, w.Then, indentation)
	return w
}
