package ast

import (
	"gitlab.com/dalibo/transqlate/lexer"
)

// Statements is a list of statements until EOF.
type Statements struct {
	Statements []Node
	EOF        lexer.Token // Holds trailing spaces and comments.
}

func (s Statements) Tokens() []lexer.Token {
	t := []lexer.Token{}
	for _, s := range s.Statements {
		t = append(t, s.Tokens()...)
	}
	t = append(t, s.EOF)
	return t
}

func (s Statements) indent(indentation string) Node {
	for i, item := range s.Statements {
		s.Statements[i] = Indent(item, indentation)
	}
	return s
}

// TransformStart applies a transformation to the first token of the node.
func (s Statements) TransformStart(f Transformer[lexer.Token]) Node {
	s.Statements[0] = s.Statements[0].TransformStart(f)
	return s
}

func (s Statements) TransformEnd(f Transformer[lexer.Token]) Node {
	last := len(s.Statements) - 1
	s.Statements[last] = s.Statements[last].TransformEnd(f)
	return s
}

func (s Statements) Visit(f Transformer[Node]) Node {
	s2 := Statements{
		EOF: s.EOF,
	}
	for _, s := range s.Statements {
		s = s.Visit(f)
		if s != nil {
			s2.Statements = append(s2.Statements, s)
		}
	}
	return f(s2)
}

// Statement is a single expression followed by a semi-colon.
type Statement struct {
	Expression Node
	End        lexer.Token // The final semi-colon.
}

func (s Statement) Tokens() []lexer.Token {
	t := []lexer.Token{}
	t = append(t, s.Expression.Tokens()...)
	t = append(t, s.End)
	return t
}

func (s Statement) TransformStart(f Transformer[lexer.Token]) Node {
	s.Expression = s.Expression.TransformStart(f)
	return s
}

func (s Statement) TransformEnd(f Transformer[lexer.Token]) Node {
	s.End = f(s.End)
	return s
}

func (s Statement) Visit(f Transformer[Node]) Node {
	s.Expression = s.Expression.Visit(f)
	return f(s)
}

func (s Statement) indent(indentation string) Node {
	s.Expression = Indent(s.Expression, indentation)
	return s
}
