package ast

import (
	"fmt"

	"gitlab.com/dalibo/transqlate/lexer"
)

// Node is the base interface for all nodes in the AST.
type Node interface {
	lexer.Tokener
	// Transform first token of the node.
	TransformStart(Transformer[lexer.Token]) Node
	// Transform last token of the node.
	TransformEnd(Transformer[lexer.Token]) Node
	// Depth-first traversal of the AST applying a transformation function.
	// Used to rewrite expressions.
	Visit(Transformer[Node]) Node
}

// Transformer is a function that transforms a value into another.
type Transformer[T any] func(T) T

// notNil returns zero value instead of nil
//
// Useful for node member that is not an interface.
// See [ast.Select] as an example.
func notNil[T Node](n Node, target *T) {
	var zero T
	if n == nil {
		*target = zero
		return
	}
	*target = n.(T)
}

// Copy recursively a node.
//
// Token lists are duplicated, but tokens are not.
func Copy[T Node](n T) T {
	return n.Visit(func(n Node) Node {
		return n
	}).(T)
}

// Leaf is a single token node without children.
type Leaf struct {
	Token lexer.Token
}

func (a Leaf) String() string {
	return a.Token.Str
}

// IsIdentifier reports whether the token is an identifier.
func (a Leaf) IsIdentifier() bool {
	return a.Token.Type == lexer.Identifier
}

func (a Leaf) Tokens() []lexer.Token {
	return []lexer.Token{a.Token}
}

func (a Leaf) TransformStart(f Transformer[lexer.Token]) Node {
	a.Token = f(a.Token)
	return a
}

func (a Leaf) TransformEnd(f Transformer[lexer.Token]) Node {
	a.Token = f(a.Token)
	return a
}

func (a Leaf) Visit(f Transformer[Node]) Node {
	return f(a)
}

// In reports whether the leaf is any of the given strings.
func (a Leaf) In(s ...any) bool {
	me := a.String()
	for _, s := range s {
		switch v := s.(type) {
		case string:
			if me == v {
				return true
			}
		case fmt.Stringer:
			if me == v.String() {
				return true
			}
		default:
			panic(fmt.Sprintf("unexpected type %T", s))
		}
	}
	return false
}

func (a Leaf) indent(indentation string) Node {
	a.Token = a.Token.Indent(indentation)
	return a
}

// Clause holds keywords prefixing an expression.
//
// For example, PARTITION BY, DISTINCT ON, etc.
type Clause struct {
	Keywords   []lexer.Token
	Expression Node // May be nil
}

func (c Clause) IsZero() bool {
	return len(c.Keywords) == 0
}

func (c Clause) Tokens() []lexer.Token {
	t := append([]lexer.Token{}, c.Keywords...)
	if c.Expression != nil {
		t = append(t, c.Expression.Tokens()...)
	}
	return t
}

func (c Clause) Visit(f Transformer[Node]) Node {
	if c.Expression != nil {
		c.Expression = c.Expression.Visit(f)
	}
	return f(c)
}

func (c Clause) TransformStart(f Transformer[lexer.Token]) Node {
	c.Keywords[0] = f(c.Keywords[0])
	return c
}

func (c Clause) TransformEnd(f Transformer[lexer.Token]) Node {
	if c.Expression != nil {
		c.Expression = c.Expression.TransformEnd(f)
	} else {
		last := len(c.Keywords) - 1
		c.Keywords[last] = f(c.Keywords[last])
	}
	return c
}
