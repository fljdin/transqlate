package ast

import "gitlab.com/dalibo/transqlate/lexer"

// With holds Common Table Expressions.
type With struct {
	With      lexer.Token
	Recursive lexer.Token
	CTEs      []CTE
}

func (w With) IsZero() bool {
	return w.With.IsZero() && len(w.CTEs) == 0
}

func (w With) Tokens() []lexer.Token {
	t := []lexer.Token{w.With, w.Recursive}
	for _, cte := range w.CTEs {
		t = append(t, cte.Tokens()...)
	}
	return t
}

func (w With) TransformStart(f Transformer[lexer.Token]) Node {
	w.With = f(w.With)
	return w
}

func (w With) TransformEnd(f Transformer[lexer.Token]) Node {
	last := len(w.CTEs) - 1
	w.CTEs[last] = w.CTEs[last].TransformEnd(f).(CTE)
	return w
}

func (w With) Visit(f Transformer[Node]) Node {
	for i, cte := range w.CTEs {
		w.CTEs[i] = cte.Visit(f).(CTE)
	}
	return f(w)
}

func (w With) indent(indentation string) Node {
	w.With = w.With.Indent(indentation)
	var before lexer.Token
	if w.Recursive.IsZero() {
		w.With = w.With.TrimNewLine()
		before = w.With
	} else {
		w.Recursive = w.Recursive.TrimNewLine()
		before = w.Recursive
	}
	for i, cte := range w.CTEs {
		w.CTEs[i], before = IndentAfter(before, cte, indentation)
	}
	return w
}

// CTE holds a single Common Table Expression.
//
// Example:
//
//	cte_name [( column_name [, ...] )]
//	AS ( query )
//	[SEARCH]
//	[CYCLE]
//
// Holds the final comma separing from the next CTE.
//
// cf. https://www.postgresql.org/docs/current/queries-with.html#QUERIES-WITH
type CTE struct {
	Name         lexer.Token
	Columns      Columns // May be zero.
	As           lexer.Token
	Materialized []lexer.Token // May be nil
	Open         lexer.Token
	Query        Node
	Close        lexer.Token
	Search       Node        // May be nil
	Cycle        Node        // May be nil
	Comma        lexer.Token // May be zero
}

func (c CTE) Tokens() []lexer.Token {
	t := []lexer.Token{c.Name}
	if !c.Columns.IsZero() {
		t = append(t, c.Columns.Tokens()...)
	}
	t = append(t, c.As)
	t = append(t, c.Materialized...)
	t = append(t, c.Open)
	t = append(t, c.Query.Tokens()...)
	t = append(t, c.Close)
	if c.Search != nil {
		t = append(t, c.Search.Tokens()...)
	}
	if c.Cycle != nil {
		t = append(t, c.Cycle.Tokens()...)
	}
	if !c.Comma.IsZero() {
		t = append(t, c.Comma)
	}
	return t
}

func (c CTE) TransformStart(f Transformer[lexer.Token]) Node {
	c.Name = f(c.Name)
	return c
}

func (c CTE) TransformEnd(f Transformer[lexer.Token]) Node {
	if c.Comma.IsZero() {
		c.Comma = f(c.Comma)
	} else if c.Cycle != nil {
		c.Cycle = c.Cycle.TransformEnd(f)
	} else if c.Search != nil {
		c.Search = c.Search.TransformEnd(f)
	} else {
		c.Close = f(c.Close)
	}
	return c
}

func (c CTE) Visit(f Transformer[Node]) Node {
	if !c.Columns.IsZero() {
		notNil(c.Columns.Visit(f), &c.Columns)
	}
	c.Query = c.Query.Visit(f)
	if c.Search != nil {
		c.Search = c.Search.Visit(f)
	}
	if c.Cycle != nil {
		c.Cycle = c.Cycle.Visit(f)
	}
	return f(c)
}

func (c CTE) indent(indentation string) Node {
	c.Name = c.Name.Indent(indentation)
	c.Open = c.Open.EndLine()
	c.Query = EndLine(Indent(c.Query, indentation+"    "))
	c.Close = c.Close.Indent(indentation)
	before := c.Close
	if c.Search != nil {
		c.Search, before = IndentAfter(before, c.Search, indentation)
	}
	if c.Cycle != nil {
		c.Cycle, _ = IndentAfter(before, c.Cycle, indentation)
	}
	if !c.Comma.IsZero() {
		c.Comma = c.Comma.EndLine()
	}
	return c
}

// Search holds the SEARCH clause of CTE.
type Search struct {
	Search  []lexer.Token
	Columns Columns
	Set     lexer.Token
	Target  lexer.Token
}

func (s Search) Tokens() []lexer.Token {
	t := s.Search
	t = append(t, s.Columns.Tokens()...)
	t = append(t, s.Set)
	return append(t, s.Target)
}

func (s Search) TransformStart(f Transformer[lexer.Token]) Node {
	s.Search[0] = f(s.Search[0])
	return s
}

func (s Search) TransformEnd(f Transformer[lexer.Token]) Node {
	s.Target = f(s.Target)
	return s
}

func (s Search) Visit(f Transformer[Node]) Node {
	s.Columns = s.Columns.Visit(f).(Columns)
	return f(s)
}

// Cycle holds CYCLE clause of CTE.
type Cycle struct {
	Cycle   lexer.Token
	Columns Columns
	Set     lexer.Token
	Target  lexer.Token

	// May be zeros
	To           lexer.Token
	Value        lexer.Token
	Default      lexer.Token
	DefaultValue lexer.Token

	// Required.
	Using lexer.Token
	Path  lexer.Token
}

func (c Cycle) Tokens() []lexer.Token {
	t := []lexer.Token{c.Cycle}
	t = append(t, c.Columns.Tokens()...)
	t = append(t, c.Set)
	t = append(t, c.Target)
	if !c.To.IsZero() {
		t = append(
			t,
			c.To, c.Value,
			c.Default, c.DefaultValue,
		)
	}
	t = append(t, c.Using)
	return append(t, c.Path)
}

func (c Cycle) TransformStart(f Transformer[lexer.Token]) Node {
	c.Cycle = f(c.Cycle)
	return c
}

func (c Cycle) TransformEnd(f Transformer[lexer.Token]) Node {
	c.Path = f(c.Path)
	return c
}

func (c Cycle) Visit(f Transformer[Node]) Node {
	c.Columns = c.Columns.Visit(f).(Columns)
	return f(c)
}

// Columns holds a list of columns names.
//
// This is plain column name, without typing like in CREATE TABLE.
//
// Parens can be zero.
type Columns struct {
	Open  lexer.Token
	Names []Column
	Close lexer.Token
}

func (c Columns) Tokens() []lexer.Token {
	t := []lexer.Token{c.Open}
	for _, n := range c.Names {
		t = append(t, n.Tokens()...)
	}
	return append(t, c.Close)
}

func (c Columns) IsZero() bool {
	return len(c.Names) == 0
}

func (c Columns) TransformStart(f Transformer[lexer.Token]) Node {
	if !c.Open.IsZero() {
		c.Open = f(c.Open)
	} else {
		c.Names[0] = c.Names[0].TransformStart(f).(Column)
	}
	return c
}

func (c Columns) TransformEnd(f Transformer[lexer.Token]) Node {
	if !c.Close.IsZero() {
		c.Close = f(c.Close)
	} else {
		last := len(c.Names) - 1
		c.Names[last] = c.Names[last].TransformEnd(f).(Column)
	}
	return c
}

func (c Columns) Visit(f Transformer[Node]) Node {
	return f(c)
}

// Column is an item in a Columns list.
type Column struct {
	Name  lexer.Token
	Comma lexer.Token
}

func (c Column) Tokens() []lexer.Token {
	t := []lexer.Token{c.Name}
	if !c.Comma.IsZero() {
		t = append(t, c.Comma)
	}
	return t
}

func (c Column) TransformStart(f Transformer[lexer.Token]) Node {
	c.Name = f(c.Name)
	return c
}

func (c Column) TransformEnd(f Transformer[lexer.Token]) Node {
	if !c.Comma.IsZero() {
		c.Comma = f(c.Comma)
	} else {
		c.Name = f(c.Name)
	}
	return c
}

func (c Column) Visit(f Transformer[Node]) Node {
	return f(c)
}
