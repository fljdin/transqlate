package ast

import (
	"slices"
	"strings"

	"gitlab.com/dalibo/transqlate/lexer"
)

// TableName guess table expression name in FROM clause.
//
// Accepts <table>, schema.<table> and expr AS <table>, ... JOIN <table>.
func TableName(expr Node) string {
	switch expr := expr.(type) {
	case Join:
		return TableName(expr.Right)
	case Alias: // X.Y AS mytable, (SELECT ...) mytable, ...
		return expr.Name.Str // Return mytable.
	case Leaf: // plain SELECT FROM mytable
		return expr.Token.Str // return mytable
	case Infix: // SELECT FROM namespace.mytable.
		if expr.Is(".") {
			return TableName(expr.Right) // Return mytable
		}
	}
	return "" // Anonymous table
}

// From holds the FROM clause of a SELECT query.
type From struct {
	From   lexer.Token
	Tables Grouping
}

func (from From) IsZero() bool {
	return from.From.IsZero() && len(from.Tables.Items) == 0
}

func (from From) Tokens() []lexer.Token {
	t := []lexer.Token{from.From}
	t = append(t, from.Tables.Tokens()...)
	return t
}

func (from From) TransformStart(f Transformer[lexer.Token]) Node {
	from.From = f(from.From)
	return from
}

func (from From) TransformEnd(f Transformer[lexer.Token]) Node {
	from.Tables = from.Tables.TransformEnd(f).(Grouping)
	return from
}

func (from From) Visit(f Transformer[Node]) Node {
	from.Tables = from.Tables.Visit(f).(Grouping)
	return f(from)
}

func (from From) indent(indentation string) Node {
	from.From = from.From.LeftIndent(indentation)
	from.Tables, _ = IndentAfter(from.From, from.Tables, indentation)
	return from
}

// Append a table expression to FROM clause.
//
// Ensures FROM keyword is set.
// Indents JOIN with FROM and tables with previous table.
// Ensures JOIN is on its own line.
func (from *From) Append(table Node) {
	if from.IsZero() {
		from.From = lexer.MustLex(" FROM")[0]
	}

	_, isJoin := table.(Join)
	if isJoin {
		last := len(from.Tables.Items) - 1
		// Ensure previous table ends line to have JOIN on its own line.
		from.Tables.Items[last] = EndLine(from.Tables.Items[last])
		// Indent JOIN with FROM.
		item := IndentFrom(Postfix{Expression: table}, from)
		// Skip comma before first table.
		from.Tables.Items = append(from.Tables.Items, item)
	} else {
		// Append like a regular comma list item.
		from.Tables.Append(table)

		l := len(from.Tables.Items)
		if l < 2 {
			return
		}

		previous := from.Tables.Items[l-2]
		_, isJoin := previous.Expression.(Join)
		if !isJoin {
			return
		}

		// Indent tables with previous table.
		from.Tables.Items[l-2] = EndLine(previous)
		from.Tables.Items[l-1] = IndentFrom(from.Tables.Items[l-1], previous)
	}
}

// IsJoin reports whether a table expression is a JOIN.
//
// table is the name of the table in the FROM clause.
func (from From) IsJoin(table string) bool {
	index := from.Index(table)
	if index == -1 {
		return false
	}
	_, isJoin := from.Tables.Items[index].Expression.(Join)
	return isJoin
}

// Replace a table expression by name.
func (from *From) Replace(table string, expr Node) {
	index := from.Index(table)
	if index == -1 {
		panic("unknown table")
	}
	from.Tables.Items[index].Expression = expr

	_, isJoin := expr.(Join)
	if isJoin { // Void comma before join.
		if index == 0 {
			panic("putting JOIN first in FROM clause")
		}
		previous := from.Tables.Items[index-1]
		if !previous.Token.IsZero() {
			// Replacing a table with a JOIN. Remove previous comma.
			previous.Expression = SpaceFrom(previous.Expression, previous)
			previous.Token = lexer.Token{}
			from.Tables.Items[index-1] = previous
		}
	} else if index > 0 {
		previous := from.Tables.Items[index-1]
		if previous.Token.IsZero() {
			// Replacing a JOIN with a table. Ensure comma before table.
			previous.Token = lexer.MustLex(",")[0]
			previous = SpaceFrom(previous, previous.Expression)
		}
		_, isJoin := previous.Expression.(Join)
		if isJoin {
			// Ensure previous JOIN is on its own line.
			previous = EndLine(previous)
		}
		from.Tables.Items[index-1] = previous
	}
}

// Names of all table expression in FROM clause.
//
// Returns anonymous expression as empty string.
func (from From) Names() (names []string) {
	for _, item := range from.Tables.Items {
		names = append(names, TableName(item.Expression))
	}
	return
}

// Contains checks existence of table expression in FROM.
func (from From) Contains(table string) bool {
	return from.Index(table) != -1
}

// Index return position of table in FROM or -1.
func (from From) Index(table string) int {
	for i, item := range from.Tables.Items {
		if TableName(item.Expression) == table {
			return i
		}
	}
	return -1
}

// Sort table names in the order they appear in FROM clause.
//
// Panics if a name is not in FROM.
func (from From) Sort(names ...string) (sorted []string) {
	for _, item := range from.Tables.Items {
		if slices.Contains(names, TableName(item.Expression)) {
			sorted = append(sorted, TableName(item.Expression))
		}
	}

	if len(sorted) != len(names) {
		panic("missing table in FROM clause")
	}

	return
}

// Join holds an explicit JOIN clause.
type Join struct {
	// Left table is previous table in From.
	Join      []lexer.Token
	Right     Node // Right table, not join direction.
	Condition Node // nil or an [ast.Where] clause with ON or USING keyword.
}

func (j Join) Tokens() (tokens []lexer.Token) {
	tokens = append(tokens, j.Join...)
	tokens = append(tokens, j.Right.Tokens()...)
	if j.Condition != nil {
		tokens = append(tokens, j.Condition.Tokens()...)
	}
	return
}

func (j Join) TransformStart(f Transformer[lexer.Token]) Node {
	j.Join[0] = f(j.Join[0])
	return j
}

func (j Join) TransformEnd(f Transformer[lexer.Token]) Node {
	if j.Condition == nil {
		j.Right = j.Right.TransformEnd(f)
	} else {
		j.Condition = j.Condition.TransformEnd(f)
	}
	return j
}

func (j Join) Visit(f Transformer[Node]) Node {
	j.Right = j.Right.Visit(f)
	if j.Condition != nil {
		j.Condition = j.Condition.Visit(f)
	}
	return f(j)
}

func (j Join) indent(indentation string) Node {
	if len(j.Join) == 1 {
		j.Join[0] = j.Join[0].LeftIndent(indentation)
	} else {
		j.Join[0] = j.Join[0].Indent(indentation)
	}
	before := j.Join[len(j.Join)-1]
	j.Right, before = IndentAfter(before, j.Right, indentation)
	if j.Condition != nil {
		if len(j.Join) > 1 {
			keyword, _ := lexer.Edges(j.Condition) // ON or USING
			// Push * JOIN condition after the river.
			indentation += strings.Repeat(" ", len(keyword.Raw)+1)
		}
		j.Condition, _ = IndentAfter(before, j.Condition, indentation)
	}
	return j
}
