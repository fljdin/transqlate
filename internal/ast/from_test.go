package ast_test

import (
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/internal/ast"
	"gitlab.com/dalibo/transqlate/internal/parser"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestTableName(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("toto")
	r.Equal("toto", ast.TableName(n))

	n = parser.MustParse("namespace.tata")
	r.Equal("tata", ast.TableName(n))

	n = parser.MustParse("SELECT * FROM toto AS titi")
	s := n.(ast.Select)
	r.Equal("titi", ast.TableName(s.From.Tables.Items[0].Expression))
}

func TestFromContains(t *testing.T) {
	r := require.New(t)

	s := parser.MustParse("SELECT 1").(ast.Select)
	r.False(s.From.Contains("toto"))

	s = parser.MustParse("SELECT * FROM a, a c, a AS b, (SELECT 1)").(ast.Select)
	r.True(s.From.Contains("a"))
	r.True(s.From.Contains("b"))
	r.True(s.From.Contains("c"))
	r.True(s.From.Contains(""))
	r.False(s.From.Contains("toto"))
}

func TestFromNames(t *testing.T) {
	r := require.New(t)

	s := parser.MustParse("SELECT 1").(ast.Select)
	r.Len(s.From.Names(), 0)

	s = parser.MustParse("SELECT * FROM a, a c, a AS b").(ast.Select)
	r.Equal([]string{"a", "c", "b"}, s.From.Names())
}

func TestFromIndex(t *testing.T) {
	r := require.New(t)

	s := parser.MustParse("SELECT 1").(ast.Select)
	r.Equal(-1, s.From.Index("toto"))

	s = parser.MustParse("SELECT * FROM a, a c, a AS b").(ast.Select)
	r.Equal(-1, s.From.Index("toto"))
	r.Equal(0, s.From.Index("a"))
	r.Equal(1, s.From.Index("c"))
	r.Equal(2, s.From.Index("b"))
}

func TestFromSort(t *testing.T) {
	r := require.New(t)

	s := parser.MustParse("SELECT * FROM a, a c, a AS b").(ast.Select)
	sorted := s.From.Sort("c", "a")
	r.Equal("a", sorted[0])
	r.Equal("c", sorted[1])

	r.Panics(func() {
		s.From.Sort("c", "toto")
	})
}

func TestFromAppend(t *testing.T) {
	r := require.New(t)

	s := parser.MustParse("SELECT 1").(ast.Select)
	s.From.Append(parser.MustParse(" toto"))
	r.Equal("SELECT 1 FROM toto", lexer.Write(s))
	s.From.Append(ast.AliasTo(parser.MustParse(" tata"), "titi"))
	r.Equal(`SELECT 1 FROM toto, tata AS "titi"`, lexer.Write(s))
}

func TestFromAppendJoin(t *testing.T) {
	r := require.New(t)

	s := parser.MustParse(dedent.Dedent(`
	SELECT 1
	FROM a JOIN b`)).(ast.Select)
	join := ast.Join{
		Join:  lexer.MustLex("JOIN"),
		Right: parser.MustParse(" c -- comment"),
	}
	s.From.Append(join)

	// JOIN is on its line.
	r.Equal(dedent.Dedent(`
	SELECT 1
	FROM a JOIN b
	JOIN c -- comment`), lexer.Write(s))
}
