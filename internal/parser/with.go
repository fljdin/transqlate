package parser

import (
	"gitlab.com/dalibo/transqlate/internal/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

func (p *state) parseWith() (ast.With, error) {
	defer Trace.enter()()

	var with ast.With
	with.With = p.consume()
	if p.keywords("RECURSIVE") {
		with.Recursive = p.consume()
	}

	for {
		cte, err := p.parseCTE()
		if err != nil {
			return with, err
		}
		with.CTEs = append(with.CTEs, cte)

		if cte.Comma.IsZero() {
			break
		}
	}

	return with, nil
}

func (p *state) parseCTE() (ast.CTE, error) {
	defer Trace.enter()()

	var err error
	var cte ast.CTE
	cte.Name = p.consume()
	if p.punctuations("(") {
		cte.Columns, err = p.parseColumns()
		if err != nil {
			return cte, err
		}
	}

	if !p.keywords("AS") {
		return cte, p.unexpected()
	}
	cte.As = p.consume()
	if p.keywords("MATERIALIZED") || p.keywords("NOT", "MATERIALIZED") {
		cte.Materialized = p.consumeMatched()
	}

	if !p.punctuations("(") {
		return cte, p.unexpected()
	}
	cte.Open = p.consume()
	cte.Query, err = p.parseExpression(0)
	if err != nil {
		return cte, err
	}
	if !p.punctuations(")") {
		return cte, p.unexpected()
	}
	cte.Close = p.consume()

	if p.keywords("SEARCH") {
		cte.Search, err = p.parseSearch()
		if err != nil {
			return cte, err
		}
	}

	if p.keywords("CYCLE") {
		cte.Cycle, err = p.parseCycle()
		if err != nil {
			return cte, err
		}
	}

	if p.punctuations(",") {
		cte.Comma = p.consume()
	}

	return cte, nil
}

func (p *state) parseColumns() (columns ast.Columns, err error) {
	defer Trace.enter()()

	if p.punctuations("(") {
		columns.Open = p.consume()
	}

	for p.seek(lexer.Identifier) {
		item := ast.Column{
			Name: p.consume(),
		}
		if p.punctuations(",") {
			item.Comma = p.consume()
		}
		columns.Names = append(columns.Names, item)

		if item.Comma.IsZero() {
			break
		}
	}

	if columns.Open.IsZero() {
		return
	}
	if !p.punctuations(")") {
		return columns, p.unexpected()
	}
	columns.Close = p.consume()
	return
}

func (p *state) parseSearch() (search ast.Search, err error) {
	defer Trace.enter()()

	if !p.keywords("SEARCH", "BREADTH", "FIRST", "BY") && !p.keywords("SEARCH", "DEPTH", "FIRST", "BY") {
		return search, p.unexpected()
	}
	search.Search = p.consumeMatched()
	search.Columns, err = p.parseColumns()
	if err != nil {
		return
	}
	if !p.keywords("SET") {
		return search, p.unexpected()
	}
	search.Set = p.consume()
	if !p.seek(lexer.Identifier) {
		return search, p.unexpected()
	}
	search.Target = p.consume()
	return search, err
}

func (p *state) parseCycle() (cycle ast.Cycle, err error) {
	defer Trace.enter()()

	if !p.keywords("CYCLE") {
		return cycle, p.unexpected()
	}
	cycle.Cycle = p.consume()
	cycle.Columns, err = p.parseColumns()
	if err != nil {
		return
	}
	if !p.keywords("SET") {
		return cycle, p.unexpected()
	}
	cycle.Set = p.consume()
	if !p.seek(lexer.Identifier) {
		return cycle, p.unexpected()
	}
	cycle.Target = p.consume()
	if p.keywords("TO") {
		cycle.To = p.consume()
		cycle.Value = p.consume()
		if !p.keywords("DEFAULT") {
			return cycle, p.unexpected()
		}
		cycle.Default = p.consume()
		cycle.DefaultValue = p.consume()
	}

	if !p.keywords("USING") {
		return cycle, p.unexpected()
	}
	cycle.Using = p.consume()
	if !p.seek(lexer.Identifier) {
		return cycle, p.unexpected()
	}
	cycle.Path = p.consume()
	return cycle, err
}
