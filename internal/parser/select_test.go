package parser_test

import (
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/internal/ast"
	"gitlab.com/dalibo/transqlate/internal/parser"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestBigSelect(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse(dedent.Dedent(`
	SELECT
	1, 2 AS a_b,
	func('arg'||'0') c1, -- comment
	(3 + 4) d
	FROM e, f AS g -- comment
	-- comment SELECT WHERE
	-- comment
	WHERE 1 = TRUE
	AND 2 <> FALSE`))
	s := n.(ast.Select)
	r.Equal("SELECT", s.Select[0].Raw)
	r.False(s.From.IsZero())
	r.False(s.Where.IsZero())
}

func TestSimpleSelect(t *testing.T) {
	r := require.New(t)
	tree := parser.MustParse("select SYSDATE FROM DUAL")
	s := tree.(ast.Select)
	r.Equal("select", s.Select[0].Raw)
	i := s.List.(ast.Leaf)
	r.Equal("SYSDATE", i.Token.Raw)
	r.Equal("DUAL", s.From.Tables.Items[0].Expression.(ast.Leaf).Token.Raw)
}

func TestStar(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("SELECT *")
	s := n.(ast.Select)
	r.Equal("*", s.List.(ast.Leaf).Token.Raw)

	n = parser.MustParse("SELECT t.*")
	s = n.(ast.Select)
	col0 := s.List.(ast.Infix)
	r.Equal(".", col0.Op[0].Raw)
	star := col0.Right.(ast.Leaf)
	r.Equal("*", star.Token.Raw)
}
func TestSelectAll(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("SELECT ALL 1")
	s := n.(ast.Select)
	r.Equal("ALL", s.Select[1].Raw)
}

func TestDistinct(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("SELECT DISTINCT 1")
	s := n.(ast.Select)
	r.False(s.Distinct.IsZero())
}

func TestDistinctOn(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("SELECT DISTINCT ON (col) 1")
	s := n.(ast.Select)
	r.Equal("ON", s.Distinct.Keywords[1].Raw)
	r.Len(s.Distinct.Expression.(ast.Grouping).Items, 1)
}

func TestFromSingle(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("SELECT 1 FROM t")
	s := n.(ast.Select)
	r.Equal("t", s.From.Tables.Items[0].Expression.(ast.Leaf).Token.Raw)
	r.Equal("SELECT 1 FROM t", lexer.Write(s))
}

func TestFromMulti(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("SELECT 1 FROM toto, tata")
	s := n.(ast.Select)
	names := s.From.Names()
	r.Equal(2, len(names))
	r.Equal("toto", names[0])
	r.Equal("tata", names[1])
	r.Equal("SELECT 1 FROM toto, tata", lexer.Write(s))
}

func TestFromSubQuery(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("SELECT 1 FROM (SELECT 1 AS col) JOIN a")
	s := n.(ast.Select)
	r.Len(s.From.Tables.Items, 2)
	r.Equal(0, s.From.Index(""))
	r.Equal(1, s.From.Index("a"))
}

func TestWhereSimple(t *testing.T) {
	r := require.New(t)
	s := parser.MustParse("SELECT 1 FROM t WHERE 2 > 3").(ast.Select)
	r.Len(s.Where.Conditions, 1)
	r.Equal("SELECT 1 FROM t WHERE 2 > 3", lexer.Write(s))
}

func TestWhereAnd(t *testing.T) {
	r := require.New(t)
	s := parser.MustParse("SELECT 1 FROM t WHERE 2 AND 3 AND 4").(ast.Select)
	r.Len(s.Where.Conditions, 3)
	r.Equal("SELECT 1 FROM t WHERE 2 AND 3 AND 4", lexer.Write(s))
}

func TestWhereOrAnd(t *testing.T) {
	r := require.New(t)
	s := parser.MustParse("SELECT 1 FROM t WHERE 2 AND 3 OR 4").(ast.Select)
	r.Len(s.Where.Conditions, 1) // OR swallows the AND
	r.Equal("SELECT 1 FROM t WHERE 2 AND 3 OR 4", lexer.Write(s))
}

func TestUnion(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("SELECT 1 UNION SELECT 2 UNION ALL SELECT 3")
	op := n.(ast.Infix)
	r.True(op.Is("UNION"))
}

func TestJoin(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("SELECT * FROM a JOIN b")
	select_ := n.(ast.Select)
	join := select_.From.Tables.Items[1].Expression.(ast.Join)
	r.Len(join.Join, 1)
	r.Equal("JOIN", join.Join[0].Str)
	r.Nil(join.Condition)
}

func TestJoinOn(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("SELECT * FROM a NATURAL JOIN b ON a.id = b.id")
	select_ := n.(ast.Select)
	join := select_.From.Tables.Items[1].Expression.(ast.Join)
	r.Len(join.Join, 2)
	r.Equal("NATURAL", join.Join[0].Str)
	on := join.Condition.(ast.Where)
	r.Equal("ON", on.Keyword.Str)
}

func TestJoinUsing(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("SELECT * FROM a FULL OUTER JOIN b USING (id)")
	select_ := n.(ast.Select)
	join := select_.From.Tables.Items[1].Expression.(ast.Join)
	r.Len(join.Join, 3)
	r.Equal("FULL", join.Join[0].Str)
	using := join.Condition.(ast.Prefix)
	r.Equal("USING", using.Token.Str)
}

func TestJoinChain(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("SELECT * FROM a JOIN b JOIN c")
	select_ := n.(ast.Select)
	r.Len(select_.From.Tables.Items, 3)
	join := select_.From.Tables.Items[1].Expression.(ast.Join)
	r.Len(join.Join, 1)
	r.Equal("JOIN", join.Join[0].Str)
	r.Nil(join.Condition)
}

func TestCountDistinct(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("SELECT COUNT(DISTINCT a) FROM b")
	s := n.(ast.Select)
	count := s.List.(ast.Call)
	r.Len(count.Args.Items, 1)
	distinct := count.Args.Items[0].Expression.(ast.Prefix)
	r.Equal("DISTINCT", distinct.Token.Str)
	column := distinct.Expression.(ast.Leaf)
	r.True(column.In("a"))
}
