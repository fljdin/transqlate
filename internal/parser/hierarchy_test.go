package parser_test

import (
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/internal/ast"
	"gitlab.com/dalibo/transqlate/lexer"
	"gitlab.com/dalibo/transqlate/internal/parser"
)

func TestConnectBy(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse(dedent.Dedent(`
	SELECT empno, ename, job, mgr
	FROM emp
	START WITH mgr IS NULL
	CONNECT BY PRIOR empno = mgr
	`))
	s := n.(ast.Select).Hierarchy.(ast.StartWith)
	r.NotNil(s.Condition)
	r.Len(s.StartWith, 2)
	r.NotNil(s.ConnectBy)
	r.Equal("CONNECT BY PRIOR empno = mgr\n", lexer.Write(s.ConnectBy))
}

func TestStartWith(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse(dedent.Dedent(`
	SELECT empno, ename, job, mgr
	FROM emp
	CONNECT BY PRIOR empno = mgr
	START WITH mgr IS NULL
	`))
	c := n.(ast.Select).Hierarchy.(ast.ConnectBy)
	r.NotNil(c.Condition)
	r.Len(c.ConnectBy, 2)
	r.NotNil(c.StartWith)
	r.Equal("START WITH mgr IS NULL\n", lexer.Write(c.StartWith))
}
