package rewrite_test

import (
	"errors"
	"log/slog"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/internal/ast"
	"gitlab.com/dalibo/transqlate/internal/parser"
	"gitlab.com/dalibo/transqlate/lexer"
	"gitlab.com/dalibo/transqlate/rewrite"
)

func TestMain(m *testing.M) {
	slog.SetDefault(slog.New(slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
		Level: slog.LevelDebug,
	})))
	os.Exit(m.Run())
}

// erroring is a mock rule always failing but always return the input node.
type erroring struct {
	Panic bool
	Err   error
}

func (erroring) String() string {
	return "always fail"
}

func (erroring) Match(n ast.Node) bool {
	return true
}

func (e erroring) Rewrite(n ast.Node) (ast.Node, error) {
	if e.Panic {
		panic(e.Err)
	}
	return n, e.Err
}

func TestError(t *testing.T) {
	r := require.New(t)

	engine := rewrite.New(
		"test",
		func(e *rewrite.Engine) {
			e.Append(
				erroring{Err: errors.New("error 1")},
				erroring{Panic: true, Err: errors.New("error 2")},
			)
		},
	)

	n, err := engine.Run(parser.MustParse("toto"))
	r.NotNil(n)
	_ = n.(ast.Leaf)
	r.Error(err)

	errs := err.(interface{ Unwrap() []error }).Unwrap()
	r.Len(errs, 2)
	r.ErrorContains(errs[0], "error 1")
	r.ErrorContains(errs[1], "panic:")
	r.ErrorContains(errs[1], "error 2")

	r.Equal("toto", lexer.Write(n), "ast.Node is not preserved.")

	rerr := errs[1].(rewrite.Err)
	r.True(rerr.Rule.(erroring).Panic)

	start, _ := lexer.Edges(n)
	r.NotNil(start.Error, "error is not attached to first token")
}
