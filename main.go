// transqlate converts SQL from a dialect to PostgreSQL.
//
// Usage: transqlate [flags] [file]
//
// See --help for details.
//
// See https://pkg.go.dev/gitlab.com/dalibo/transqlate for API documentation.
package main

import (
	"gitlab.com/dalibo/transqlate/internal"
)

var (
	version = "*unreleased*"
)

func main() {
	internal.Transqlate(version)
}
