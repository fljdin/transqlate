# Contributing to transqlate

Prerequisites:

- Go 1.21
- golangci-lint

Use go as usual to build and test your code.


## Dumping AST

Use transqlate CLI to expose the parser walk,
the AST of an SQL snippet as parsed by transqlate
and the render of the AST.

``` console
$ go run .
Type SQL snippet and terminate with ^D
SELECT 1 + 2
14:32:18 DEBUG  Parsing SQL.                     source=-
enter: parseSnippet
  enter: parseStatement
    enter: parseExpression precedence=0
      enter: parseSelect
        consume: Keyword "SELECT"
        enter: parseExpression precedence=0
          enter: parseLeaf
            consume: Integer "1"
          enter: parseInfix
            consume: Operator "+"
            enter: parseExpression precedence=30
              enter: parseLeaf
                consume: Integer "2"
  consume: EOF ""
14:32:18 DEBUG  Dumping AST.                     dialect=oracle
ast.Select:
┊ Select: Keyword "SELECT"
┊ List ast.Infix:
┊ ┊ Left ast.Leaf: Integer "1"
┊ ┊ Op: Operator "+"
┊ ┊ Right ast.Leaf: Integer "2"
14:32:21 DEBUG  Serialize SQL.                   dialect=postgres
SELECT 1 + 2
```

A bash oneliner to transpile a simple query is:

``` console
$ transqlate <<<"SELECT 1 + 2"
```


## Parser and AST Design

The purpose of transqlate is to edit code.
Not to regenerate it.

This is why there are very few token types.
transqlate don't bother to represent `SELECT` keyword with a constant integer from a big enumeration of every token.
This is for interpreters.
For transqlate, `SELECT` is a token of type keyword.
If some code needs to checks the value of a token, it must use it's normalized value.

The first purpose of AST is to reassemble the code as it was written.
One goal of transqlate is to keep the implementation simple.
Since Go is more about composition than inheritance,
transqlate reuse node struct for multiple expressions.
The simplest example is `ast.Infix` which can hold any infix operation : `+`, `AND`, `NOT LIKE`, etc.

Sometimes, it is better to represent code not logically but literaly.
For example, JOINs are represented as a list in transqlate instead of a hierarchy.

AST nodes provides helpers to test node content,
edit code or whitespaces,
and build new code.


## SQL Style

transqlate generates code.
It also prettifies code if asked.
transqlate can't infer style of existing code.
Generated code will always be in transqlate style.

transqlate uses [Simon Holywell SQL Style](https://www.sqlstyle.guide/).
Use space to indent.


## Releasing

Push to master a tag prefixed with `v` and containing a Semantic Version.
GitLab CI job `release` will do the work.


## Go references

- [Effective Go](https://go.dev/doc/effective_go).
- [Go Doc Comments](https://tip.golang.org/doc/comment).
- [Idiomatic Go](https://dmitri.shuralyov.com/idiomatic-go) from @dmitshur, Googler.
- [Idiomatic Go](https://www.youtube.com/watch?v=9cJHCoSxbn8) from Anthony GG (video).
