package lexer_test

import (
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestCommentEmpty(t *testing.T) {
	r := require.New(t)
	i := lexer.MustLex("a")[0]
	i = i.Comment("toto")
	r.Equal("-- toto\na", lexer.Write(lexer.Tokens{i}))
}

func TestCommentIndentated(t *testing.T) {
	r := require.New(t)
	i := lexer.MustLex("  a")[0]
	i = i.Comment("toto")
	r.Equal("  -- toto\n  a", lexer.Write(lexer.Tokens{i}))

	i = lexer.MustLex("\ta")[0]
	i = i.Comment("toto")
	r.Equal("\t-- toto\n\ta", lexer.Write(lexer.Tokens{i}))
}

func TestCommentPreserved(t *testing.T) {
	r := require.New(t)
	i := lexer.MustLex("  -- preserved\n  a")[0]
	i = i.Comment("toto")
	r.Equal("  -- preserved\n  -- toto\n  a", lexer.Write(lexer.Tokens{i}))
}

func TestIndent(t *testing.T) {
	r := require.New(t)
	i := lexer.MustLex(dedent.Dedent(`
	-- comment

	-- comment
	a
	`)[1:])[0]
	i = i.Indent("\t")
	r.Equal(""+ // for gofmt to align following strings
		"\t-- comment\n"+
		"\n"+
		"\t-- comment\n"+
		"\ta\n",
		lexer.Write(lexer.Tokens{i}))
}

func TestLeftIndent(t *testing.T) {
	r := require.New(t)
	i := lexer.MustLex("AND")[0]
	i = i.LeftIndent("      ")
	r.Equal("  AND", lexer.Write(lexer.Tokens{i}))
}

func TestLeftIndentLonger(t *testing.T) {
	r := require.New(t)
	i := lexer.MustLex("HAVING")[0]
	i = i.LeftIndent("  ")
	r.Equal("  HAVING", lexer.Write(lexer.Tokens{i}))
}

func TestGuessIndentation(t *testing.T) {
	r := require.New(t)
	i := lexer.MustLex(dedent.Dedent(`
	-- comment
		a
	`))[0]
	r.Equal("\t", i.Indentation())
}

func TestIncrementIndentation(t *testing.T) {
	r := require.New(t)
	i := lexer.IncreaseIndentation("", "   WHEN   ")
	r.Equal("     ", i)
	//       WHEN_
}
