package lexer_test

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestWriteNil(t *testing.T) {
	r := require.New(t)
	r.Equal("", lexer.Write(nil))
}

func TestLineAt(t *testing.T) {
	r := require.New(t)
	l := lexer.New("", "1\n2 3 4\n5\n")
	var tokens lexer.Tokens
	for {
		tok := l.Next()
		tokens = append(tokens, tok)
		if tok.Type == lexer.EOF {
			break
		}
	}
	r.Len(tokens, 6)

	r.Equal("3", tokens[2].Raw)
	sql := tokens.LineAt(2)
	r.Equal("2 3 4\n", lexer.Write(sql))
}

func TestShortWrite(t *testing.T) {
	r := require.New(t)
	l := lexer.MustLex("SELECT * FROM")
	r.Equal("SELECT * FROM", lexer.ShortWrite(lexer.Tokens(l)))

	l = lexer.MustLex("SELECT *\n FROM")
	r.Equal("SELECT *...", lexer.ShortWrite(lexer.Tokens(l)))

	l = lexer.MustLex("   SELECT * FROM very, long, query, with, many, tables, and, joins, and, conditions, and, comments, and, subqueries, and, functions, and, operators, and, expressions")
	s := lexer.ShortWrite(lexer.Tokens(l))
	r.False(strings.HasPrefix(s, "  "))
	r.True(strings.HasSuffix(s, "..."))
}
