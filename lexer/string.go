package lexer

import "strings"

// ApplyCase applies the case of orig to s.
//
// Supports only lower and upper case. Not mixed case.
func ApplyCase(orig, s string) string {
	if strings.ToLower(orig) == orig {
		return strings.ToLower(s)
	}
	if strings.ToUpper(orig) == orig {
		return strings.ToUpper(s)
	}
	return s
}

// QuoteString creates a new string token.
//
// The raw string is quoted and escaped.
func QuoteString(raw string) string {
	return `'` + strings.ReplaceAll(raw, `'`, `''`) + `'`
}

// UnquoteString unquotes and unescapes a string.
func UnquoteString(raw string) string {
	if raw[0] != '\'' {
		return raw
	}
	return strings.ReplaceAll(raw[1:len(raw)-1], "''", "'")
}

func QuoteIdentifier(raw string) string {
	return `"` + strings.ReplaceAll(raw, `"`, `""`) + `"`
}

func UnquoteIdentifier(raw string) string {
	if raw[0] != '"' {
		return raw
	}
	return strings.ReplaceAll(raw[1:len(raw)-1], `""`, `"`)
}
