package lexer

import (
	"errors"
	"fmt"
	"strings"
)

// errorf returns an error token and terminates the scan by passing
// back a nil pointer that will be the next state, terminating l.Next().
func (l *Lexer) errorf(format string, args ...interface{}) stateFn {
	eol := strings.Index(l.input[l.start:], "\n")
	if eol == -1 {
		// No newline in the rest of the input.
		eol = len(l.input) - l.start
	}

	partialToken := Token{
		Type:   Raw,
		Prefix: l.input[l.prefix:l.start],
		Raw:    l.input[l.start : l.start+eol],
		Line:   l.line,
		Column: l.col,
		Error:  fmt.Errorf(format, args...),
	}
	eof := Token{
		Type:   EOF,
		Line:   l.line,
		Column: partialToken.Column + len(partialToken.Raw),
	}
	e := Err{
		Source: l.Source,
		Line:   l.line,
		Column: l.col,
		LineTokens: append(
			l.lineTokens,
			partialToken,
			eof,
		),
		Err: partialToken.Error,
	}

	l.tokens <- Token{
		Type:  Error,
		Raw:   e.Err.Error(),
		Error: e,
	}
	return nil
}

// An error on a specific line and column of a source file.
//
// Also used by parser.
type Err struct {
	Source     string // Name of the input
	Line       int    // Line number in Source. Zero-based.
	Column     int    // Column number in Source. Zero-based.
	LineTokens Tokens // Tokens of offending line. Offending token is last.
	Err        error
}

func (e Err) Error() string {
	// Don't reformat our errors.
	var lerr Err
	if errors.As(e.Err, &lerr) {
		return e.Err.Error()
	}
	return fmt.Sprintf(
		"%s at +%d:%d %s",
		e.Err.Error(), 1+e.Line, 1+e.Column, e.Source,
	)
}

func (e Err) Unwrap() error {
	return e.Err
}

func (e Err) Tokens() Tokens {
	return e.LineTokens
}

// BadToken returns the token that caused the error.
func (e Err) BadToken() Token {
	for _, t := range e.Tokens() {
		if t.Line == e.Line && t.Column == e.Column {
			return t
		}
	}
	panic("no token matches error line and column")
}
