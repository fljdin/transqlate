package lexer

import "strings"

// Comment insert a comment before the token.
//
// Compute indentation from the token.
// Line and Column are not updated.
// Existing prefix is preserved.
// Handles tab or spaces indentation.
func (t Token) Comment(s string) Token {
	t.Prefix = t.Prefix + "-- " + s + "\n" + t.Indentation()
	return t
}

// SpaceFrom copies spacing from other token.
func (t Token) SpaceFrom(other Token) Token {
	t.Prefix = other.Prefix
	t.Suffix = other.Suffix
	return t
}

// Indent token.
//
// Overrides current indentation. Preserves comment and indent them as well.
// Assume token begins line.
func (t Token) Indent(indentation string) Token {
	lines := strings.Split(t.Prefix, "\n")
	for i, line := range lines {
		line = strings.TrimLeft(line, "\t ")
		if line != "" {
			line = indentation + line
		}
		lines[i] = line
	}
	t.Prefix = strings.Join(lines, "\n") + indentation
	return t
}

// LeftIndent token before the last space of indentation.
//
// This creates "river" of spaces with keyword on left and expression on right.
//
// The token is indented so that spacings + token length = length of indentation.
func (t Token) LeftIndent(indentation string) Token {
	li := len(indentation)
	lt := len(t.Raw)
	if li > lt {
		indentation = indentation[:li-lt-1]
	}
	return t.Indent(indentation)
}

// EndLine ensures token ends line.
func (t Token) EndLine() Token {
	if !t.EndsLine() {
		t.Suffix += "\n"
	}
	return t
}

// TrimNewLine ensures token does not end line.
//
// Preserves comment.
func (t Token) TrimNewLine() Token {
	if t.TrailingComment() == "" {
		t.Suffix = ""
	}
	return t
}

// IncreaseIndentation pad spaces to align after code
//
// First argument is starting indentation. code is the snippet after which you want to align next
// lines. e.g. code is the function name upon which you want to align all arguments after the end of
// the function.
//
// Example:
//
//	date_trunc('year',
//	           hired)
func IncreaseIndentation(i string, code string) string {
	code = strings.Trim(code, " \t")
	lines := strings.Split(code, "\n")
	if len(lines) == 1 {
		i += strings.Repeat(" ", len(code)+1)
	} else {
		last := lines[len(lines)-1]
		i = strings.Repeat(" ", len(last)+1)
	}
	return i
}
