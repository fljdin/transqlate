package lexer_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestNormalizeIdentifier(t *testing.T) {
	r := require.New(t)
	tk := lexer.Token{
		Type: lexer.Identifier,
		Raw:  `"quoted"`,
	}
	tk.Normalize()
	r.Equal("quoted", tk.Str)
}

func TestNormalizeKeyword(t *testing.T) {
	r := require.New(t)
	tk := lexer.Token{
		Type: lexer.Keyword,
		Raw:  `select`,
	}
	tk.Normalize()
	r.Equal("SELECT", tk.Str)
}

func TestNormalizeInteger(t *testing.T) {
	r := require.New(t)
	tk := lexer.MustLex(`123`)[0]
	tk.Normalize()
	r.Equal(123, tk.Int)

	tk = lexer.MustLex(`x'ff'`)[0]
	tk.Normalize()
	r.Equal(255, tk.Int)
}

func TestBeginDetection(t *testing.T) {
	r := require.New(t)
	tk := lexer.MustLex("SELECT 1")[0]
	r.True(tk.BeginsLine())

	tk = lexer.Token{
		Prefix: "  ",
		Column: 2,
	}
	r.True(tk.BeginsLine())

	tk = lexer.Token{
		Prefix: " ",
		Column: 9,
	}
	r.False(tk.BeginsLine())

	tk = lexer.Token{
		Prefix: "-- comment\n",
		Column: 0,
	}
	r.True(tk.BeginsLine())
}

func TestIndentation(t *testing.T) {
	r := require.New(t)
	tk := lexer.Token{
		Prefix: "  ",
		Column: 2,
		Raw:    "start_of_line",
	}
	r.Equal("  ", tk.Indentation())

	tk = lexer.Token{
		Prefix: " ",
		Column: 9,
		Raw:    "middle_of_line",
	}
	r.Equal("", tk.Indentation())

	tk = lexer.Token{
		Prefix: "-- comment\n  ",
		Column: 2,
		Raw:    "start_of_line",
	}
	r.Equal("  ", tk.Indentation())
}

func TestLeadingComment(t *testing.T) {
	r := require.New(t)

	tk := lexer.Token{Prefix: "\n"}
	r.Equal("", tk.LeadingComment())

	// Ensure indentation is not in comment.
	tk = lexer.Token{Prefix: "-- comment\n  "}
	r.Equal("-- comment\n", tk.LeadingComment())

	tk = lexer.Token{Prefix: "  /* comment */  "}
	r.Equal("  /* comment */", tk.LeadingComment())
}

func TestTrailingComment(t *testing.T) {
	r := require.New(t)

	tk := lexer.Token{Suffix: "  \n"}
	r.Equal("", tk.TrailingComment())

	tk = lexer.Token{Suffix: "-- comment\n"}
	r.Equal("-- comment\n", tk.TrailingComment())

	tk = lexer.Token{Suffix: "/* comment */\n"}
	r.Equal("/* comment */\n", tk.TrailingComment())
}
