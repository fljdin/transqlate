package lexer_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestDollarQuotedString(t *testing.T) {
	r := require.New(t)
	l := lexer.New("", `select $$anonymous tagged string$$`)
	r.Equal(lexer.Token{Raw: "select", Type: lexer.Keyword}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "$$anonymous tagged string$$", Type: lexer.String, Column: 7}, l.Next())

	l = lexer.New("", `select $tag$named tagged string$tag$;`)
	r.Equal(lexer.Token{Raw: "select", Type: lexer.Keyword}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "$tag$named tagged string$tag$", Type: lexer.String, Column: 7}, l.Next())

	l = lexer.New("", `select $toto$incomplete tag$to`)
	r.Equal(lexer.Token{Raw: "select", Type: lexer.Keyword}, l.Next())
	tk := l.Next()
	r.Equal(lexer.Error, tk.Type)
	r.Error(tk.Error)

	l = lexer.New("", `select $$incomplete tag$`)
	r.Equal(lexer.Token{Raw: "select", Type: lexer.Keyword}, l.Next())
	tk = l.Next()
	r.Equal(lexer.Error, tk.Type)
	r.Error(tk.Error)
}

func TestQuotedString(t *testing.T) {
	r := require.New(t)
	l := lexer.New("", `select 'string literal' as "identifier";`)
	r.Equal(lexer.Token{Raw: "select", Type: lexer.Keyword}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "'string literal'", Type: lexer.String, Column: 7}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "as", Type: lexer.Keyword, Column: 24}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: `"identifier"`, Type: lexer.Identifier, Column: 27}, l.Next())
	r.Equal(lexer.Token{Raw: ";", Type: lexer.Punctuation, Column: 39}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Column: 40}, l.Next())

	l = lexer.New("", `select 'string '' literal';`)
	r.Equal(lexer.Token{Raw: "select", Type: lexer.Keyword}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "'string '' literal'", Type: lexer.String, Column: 7}, l.Next())
	r.Equal(lexer.Token{Raw: ";", Type: lexer.Punctuation, Column: 26}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Column: 27}, l.Next())
}

func TestUnicodeString(t *testing.T) {
	r := require.New(t)
	l := lexer.New("", `N'é'`)
	r.Equal(lexer.Token{Raw: "N'é'", Type: lexer.String}, l.Next())
}

func TestQuoteString(t *testing.T) {
	r := require.New(t)
	r.Equal("''", lexer.QuoteString(""))
	r.Equal("'to''to'", lexer.QuoteString("to'to"))
}

func TestUnquoteString(t *testing.T) {
	r := require.New(t)
	r.Equal("", lexer.UnquoteString("''"))
	r.Equal("toto", lexer.UnquoteString("'toto'"))
	r.Equal("to'to", lexer.UnquoteString("'to''to'"))
}

func TestQuoteIdentifier(t *testing.T) {
	r := require.New(t)
	r.Equal(`"to""to"`, lexer.QuoteIdentifier(`to"to`))
}

func TestUnquoteIdentifier(t *testing.T) {
	r := require.New(t)
	r.Equal(`to"to`, lexer.UnquoteIdentifier(`"to""to"`))
}
