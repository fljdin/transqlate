#!/usr/bin/env bats

bats_require_minimum_version 1.5.0

@test "change-case" {
	run --separate-stderr transqlate <<<'SELECT UPPER, Mixed, lower, "QUPPER", "QMixed", "qlower"'
	[ "$status" -eq 0 ]
	[ "${lines[0]}" = 'SELECT UPPER, Mixed, lower, QUPPER, "QMixed", qlower' ]
}

@test "change-case pretty" {
	run --separate-stderr transqlate --pretty <<<'SELECT UPPER, Mixed, lower, "QUPPER", "QMixed", "qlower"'
	[ "$status" -eq 0 ]
	[ "${lines[0]}" = 'SELECT upper, mixed, lower, qupper, "QMixed", qlower' ]
}

@test "preserve-case" {
	run --separate-stderr transqlate --preserve-case <<<'SELECT UPPER, Mixed, lower, "QUPPER", "QMixed", "qlower"'
	[ "$status" -eq 0 ]
	[ "${lines[0]}" = 'SELECT "UPPER", "MIXED", "LOWER", "QUPPER", "QMixed", "qlower"' ]
}
