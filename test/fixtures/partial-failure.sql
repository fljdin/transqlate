SELECT
  1 AS a,
  -- Error on first token of the line.
  TRUNC(
    b,
    'BAD FORMAT'
  ),
  -- Error in the middle of the line.
  3 AS c, trunc(d, 'ER'||'ROR') AS d
FROM d
WHERE 1 = 1 AND 2 = 2
  -- Two errors in the same line. Indented with spaces.
  AND TRUNC(d, 'BAD FORMAT') AND TRUNC(d, compute_format());
