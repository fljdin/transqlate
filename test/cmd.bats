#!/usr/bin/env bats

@test "help" {
	transqlate --help
}

@test "version" {
	transqlate --version
}

@test "builtin" {
	transqlate @
}

@test "colors" {
	transqlate --plain=false @
}

@test "plain" {
	transqlate --plain @
}

@test "pretty" {
	transqlate --pretty <<<"select 1" | grep -q "SELECT"
}

@test "sqlstyle" {
	transqlate --pretty ./test/fixtures/unindented.sql > out.sql
	diff -u --color ./test/fixtures/indentation.expected.sql out.sql
	# out.sql is also in .gitignore
	rm -f out.sql
}

@test "re-sqlstyle" {
	transqlate --pretty ./test/fixtures/indentation.expected.sql > out.sql
	diff -u --color ./test/fixtures/indentation.expected.sql out.sql
	# out.sql is also in .gitignore
	rm -f out.sql
}
